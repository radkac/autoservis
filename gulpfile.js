var BowerWebpack = require('bower-webpack-plugin');
var gulp = require('gulp');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');
var named = require('vinyl-named');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var autoprefixer = require('gulp-autoprefixer');
var webpack = require('webpack-stream');

/**
 * CSS
 **/
gulp.task('sass', function () {
  return gulp.src('www/scss/style-*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({browsers: ['last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'], cascade: false}))
    .pipe(gulp.dest('www/css'));
});

/**
 * Minimalization
 */
gulp.task('css-min', function () {
  return gulp.src(['www/css/*.css', '!www/css/**/*.min.css'])
    .pipe(cssnano())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('www/css'));
});

/**
 * Watch
 */
gulp.task('watch', function () {
  gulp.watch('www/scss/**/*.scss', ['sass']);
  gulp.watch('www/scripts/**/*.es6', ['transpile']);
});

gulp.task('css', function(callback) {
  runSequence('sass', 'css-min', callback);
});


gulp.task('copy', function () {
  // Font Awesome Fonts
  gulp.src('www/bower_components/font-awesome/fonts/*')
      .pipe(gulp.dest('www/fonts'));

  // Bootstrap scripts

  gulp.task('copy', function() {
    return gulp.src('www/bower_components/bootstrap-sass/assets/fonts/**/*')
        .pipe(gulp.dest('www/fonts'));
  });
});


gulp.task('transpile', function() {
  var config = {
    module: {
      //name: 'main',
      loaders: [
        {
          test: /\.es6$/,
          loader: 'babel?presets[]=es2015'
        }
      ]
    },
    plugins: [
        new BowerWebpack({
          moduleDirectories: ['www/bower_components']
        })
    ]
  };

  return gulp.src(['www/scripts/app-*.es6'])
      .pipe(named())
      .pipe(webpack(config))
      .pipe(gulp.dest('www/js'));
});

/**
 * Default task
 **/

gulp.task('default', function(callback) {
  runSequence(['copy', 'css', 'transpile'], 'watch', callback);
});

  