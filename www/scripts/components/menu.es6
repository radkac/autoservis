let $ = require('jquery');

/**
 * @param {string} selectorButton
 * @param {string} selectorMenu
 */
let menu = function (selectorButton, selectorMenu) {
    let elementButton = $(selectorButton);
    let elementMenu = $(selectorMenu);

    elementButton.click(function () {
        elementButton.toggleClass('collapsed');

        elementMenu.toggleClass('in');
    });
};

module.exports = menu;