let $ = require('jquery');
/**
 *
 * @param {string} selector
 * @param {string} className
 */
let dropdown = function (selector, className = 'open') {
    let element = $(selector);
    // let isOpen = false;
    //
    // element.click(function () {
    //     if (isOpen) {
    //         element.removeClass('open');
    //         isOpen = false;
    //     }
    //     else {
    //         element.addClass('open');
    //         isOpen = true;
    //     }
    // });
    element.click(function () {
        element.toggleClass(className);
    });
};

module.exports = dropdown;