let $ = require('jquery');

/**
 * @param {object} customer
 * @returns {string}
 */
let buildRow = function(customer) {
    console.log(customer);
    let htmlOut = '<a href=' + customer.url + '>' + customer.phone + ', IČO:' + customer.ico + '</a><br />';

    return htmlOut;
};

/**
 * @param {string} selector
 * @param {string} url
 */
let search = function (selector, url) {
    let element = $(selector);
    let autocomplete = $('#autocomplete');

    console.log(url);

    element.on('keyup', function () {
        let value = $(this).val();
        let urlQuery = url + `?query=${value}`;

        $.ajax(urlQuery)
            .done(function (data) {
                let output = '';
                for(let index in data)
                {
                    let customer = data[index];
                    output += buildRow(customer);       // volam funkciu na zostavenie riadku
                }
                autocomplete.html(output);
            })
            .fail(function () {
                try {

                }  catch (err) {
                    console.dir(err);
                    console.log("Error Message!", err.message);
                    console.log("Stack Trace", err.stack);
                }
            });
    });
};

module.exports = search;