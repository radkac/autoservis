    let $ = require('jquery');
    let search = require('./components/search.es6');
    let dropdown = require('./components/dropdown.es6');
    let menu = require('./components/menu.es6');

    $(document).ready(function () {

        let searchLink = $("#search").attr('data-url');

        search('#search', searchLink);
        dropdown('#dropdown');
        menu('#menu-button', '#menu');
        //remove native browser validation
        $('input, select').removeAttr('required');
    });
