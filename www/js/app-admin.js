$( function() {
	$( "#datepicker" ).datepicker({
		dateFormat: "dd.mm.yy",
	});

    $("#datepicker").focus(function () {
        $(".ui-datepicker-calendar").show();
    });
} );

$( function() {
	$( "#datepickerMonth" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "MM yy",
		onClose: function(dateText, inst) {
			$(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
		}
	});

	$("#datepickerMonth").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	});
} );