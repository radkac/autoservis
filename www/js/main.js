$(document).ready(function() {
    $('#calendar').fullCalendar({
        events:  '/www/employee/json/bookings',
        weekends: false,
        selectHelper: true,
        editable: true,
        select: function(start, end, allDay) {
            var title = prompt('Event Title:');
            if (title) {
                calendar.fullCalendar('renderEvent',
                    {
                        title: title,
                        start: start,
                        end: end,
                        allDay: allDay
                    },
                    true // make the event "stick"
                );
                /**
                 * ajax call to store event in DB
                 */
                jQuery.post(
                    "event/new" // your url
                    , { // re-use event's data
                        title: title,
                        start: start,
                        end: end,
                        allDay: allDay
                    }
                );
            }
            calendar.fullCalendar('unselect');
        },


        eventDrop: function(event, delta, revertFunc) {

            $.post() //...
            // xxxx

        },
        eventResize(event) {
            console.log(event);
        },


        // dayClick: function() {
        //     let moment = $('#calendar').fullCalendar('getDate');
        //     // alert("The current date of the calendar is " + moment.format());
        // },
        dayClick: function(date, jsEvent, view) {
            var formatDate = date.toISOString().slice(0, 10);
            var formatTime = date.toISOString().slice(11, 16);

            console.log(formatTime);

            $('#datepicker').val(formatDate);
            $('#frm-bookingForm-bookingForm-time').val(formatTime);
            // alert('Clicked on: ' + date.format());
            //
            // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
            //
            // alert('Current view: ' + view.name);

            // change the day's background color just for fun
            // $(this).css('background-color', 'red');

        },
        // eventClick: function(calEvent, jsEvent, view) {
        //
        //     alert('Event: ' + calEvent.title);
        //     alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
        //     alert('View: ' + view.name);
        //
        //     // change the border color just for fun
        //     $(this).css('border-color', 'red');
        //
        // },
        businessHours: {
            // days of week. an array of zero-based day of week integers (0=Sunday)
            dow: [ 1, 2, 3, 4, 5 ], // Monday - Friday

            start: '08:00',
            end: '20:00',
        },
        height: 500,
        header: {
            center: 'title',
            left:  'today prev,next',
            right: 'month, agendaWeek, agendaDay'
        },
        defaultView: 'agendaWeek',
        // events: [
        //     {
        //         title:  'My Event',
        //         start:  '2017-07-17T14:30:00',
        //         allDay: false
        //     }
        //     // other events here...
        // ],
        // eventClick: function(event) {
        //     console.log(event.title);
        // },
        // eventClick: function(event, jsEvent, view) {
        //     $.getScript(event.edit_url, function() {});
        // },
        timeFormat: 'H:mm', // uppercase H for 24-hour clock
        eventColor: '#bada55',
        navLinks: true,
        dateFormat: 'dd.mm.yyyy'

    })

});