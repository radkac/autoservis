<?php

namespace App\Model;

use App\Model\Entity\User;
use Kdyby\Doctrine\EntityManager;

class UserModel extends BaseModel
{
    private $entityManager;

    /**
     * UserModel constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getUsers()
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('u');
        $query->from(User::class, 'u');

        return $query->getQuery();
    }

    /**
     * @param $userId
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getUserById($userId)
    {
        return $this->entityManager->find(User::class, $userId);
    }

    /**
     * @param string $username
     * @return User|null
     */
    public function getUserByUsername($username)
    {
        return $this->entityManager->getRepository(User::class)->findOneBy(['username eq'=>$username]);
    }

    public function getShopUsers($shopId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('u')
            ->from(User::class, 'u')
            ->where('u.shop = :shopId')
            ->andWhere('u.group = :groupId')
            ->setParameters(['shopId'=>$shopId, 'groupId'=>'3']);

        return $query->getQuery();
    }
}