<?php

namespace App\Model;

use App\Model\Entity\Order;
use App\Model\Entity\Service;
use Kdyby\Doctrine\EntityManager;

class ServiceModel extends BaseModel
{
    private $entityManager;


    /**
     * ServiceModel constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getServices()
    {
        return $this->entityManager->getRepository(Service::class)->findPairs('name');
    }

    public function getAllServices()
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('s');
        $query->from(Service::class, 's');
        return $query->getQuery();
    }

    /**
     * @param $serviceId
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getServiceById($serviceId)
    {
        return $this->entityManager->find(Service::class, $serviceId);
    }
}