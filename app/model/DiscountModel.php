<?php

namespace App\Model;


use App\Model\Entity\Customer;
use App\Model\Entity\Discount;
use App\Model\Entity\Order;
use DateTime;
use Kdyby\Doctrine\EntityManager;
use Tracy\Debugger;


class DiscountModel extends BaseModel
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get all Discounts from table Discount
     *
     * @return \Doctrine\ORM\Query
     */
    public function getDiscounts()
    {
        return $this->entityManager->getRepository(Discount::class)->findPairs('name');
    }

    public function getDiscountNames()
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('d')
            ->from(Discount::class, 'd');

        return $query->getQuery();
    }

    public function getDiscountName($name)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('d')
            ->from(Discount::class, 'd')
            ->where('d.name LIKE :name')
            ->setParameter('name', $name);

        return $query->getQuery();
    }

    public function getDiscountById($discountId)
    {
        return $this->entityManager->find(Discount::class, $discountId);
    }
}