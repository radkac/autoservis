<?php

namespace App\Model;

use App\Model\Entity\Storno;
use Kdyby\Doctrine\EntityManager;

class StornoModel extends BaseModel
{
    private $entityManager;

    /**
     * StornoModel constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllStornos()
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('s')
            ->from(Storno::class, 's')
            ->orderBy('s.datetime', 'DESC');

        return $query->getQuery();
    }
    
    /**
     * @param $stornoId
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getStornoById($stornoId)
    {
        return $this->entityManager->find(Storno::class, $stornoId);
    }

    public function getStornosByDate($date)
    {
        $formatedDate = $date->format('Y-m-d');
        $query = $this->entityManager->createQueryBuilder();
        $query->select('s')
            ->from(Storno::class, 's')
            ->where('substring(s.datetime, 1, 10) LIKE :date')
            ->distinct(true)
            ->setParameters(['date'=>$formatedDate]);

        return $query->getQuery();
    }
}