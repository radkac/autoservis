<?php

namespace App\Model;

use Nette\Object;
use Nette\Utils\DateTime;

class DateTimeModel extends BaseModel
{

    /**
     * @var DateTime
     */
    private $date;

    /**
     * @var array
     */
    private $dni = array("neděli", "pondělí", "úterý", "středu", "čtvrtek", "pátek", "sobotu");

    /**
     * @var array
     */
    private $mesiace = array("ledna", "února", "března", "dubna", "května", "června", "července", "srpna", "září", "října", "listopadu", "prosince");


//    public function __construct(DateTime $date)
//    {
//        $this->date = $date;
//    }

    /**
     * @return string
     */

    public function formatDT()
    {
        $now_date = new DateTime();
        $this->date = $now_date;
        $midnight = new DateTime();
        $midnight->modify('+1 day');
        $midnight->setTime(0,0,0);

        $today = new DateTime();
        $today->setTime(0,0,0);

        $yesterday = new DateTime();
        $yesterday->modify('yesterday');
        $yesterday->setTime(0,0,0);

        $now = new DateTime();

        $den = $this->date->format('w');
        $mesiac = $this->date->format('n') - 1;

        if($this->date < $midnight AND $this->date >= $today)
        {
            // TODO 1 hodina a 1 minuta
            $diff = $now->getTimestamp() - $this->date->getTimestamp();
            if($diff < 3600)
            {
                if($diff > 0 and $diff <= 120)
                {
                    return 'pred ' . round(floor($diff/60)) . ' minútou';
                }
                else
                {
                    return 'pred ' . round(floor($diff/60)) . ' minútami';
                }
            }
            if($diff > 3600)
            {
                if($diff > 3600 and $diff <= 7200)
                {
                    return 'pred ' . round(floor($diff/3600)) . ' hodinou';
                }
                else
                {
                    return 'pred ' . round(floor($diff/3600)) . ' hodinami';
                }
            }
            else
            {
                return 'dnes o ' . $this->date->format('H:i');
            }
        }
        if($this->date < $midnight AND $this->date >= $yesterday)
        {
            return 'včera o ' . $this->date->format('H:i');
        }

        return $this->dni[$den] . ' ' . $this->date->format('d.') . ' ' . $this->mesiace[$mesiac] . ' o ' . $this->date->format('H:i');
    }
}