<?php

namespace App\Model;


use App\Model\Entity\UserGroup;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security as NS;
use Nette\Security\Passwords;
use Nette\Utils\DateTime;
use Tracy\Debugger;

class PasswordAuthenticator extends Object implements IAuthenticator
{
    /**
     * @var UserModel
     */
    private $userModel;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(UserModel $userModel, EntityManager $entityManager)
    {
        $this->userModel = $userModel;
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $credentials
     * @return Identity
     * @throws NS\AuthenticationException
     */
    public function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;

        $user = $this->userModel->getUserByUsername($username);

        if(!$user)
        {
            throw new NS\AuthenticationException('Používateľ neexistuje');
        }
        if ($user->group->type == UserGroup::GROUP_FORMER_EMPLOYEE) {
            throw new NS\AuthenticationException('Bývalí zamestnanaci nemajú povolený vstup do administrácie. Kontaktujte administrátora systému.');
        }

        if(!Passwords::verify($password, $user->password))
        {
            throw new NS\AuthenticationException('Heslo nie je správne.');
        }

        $user->lastLogin = new DateTime();
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        
        return new NS\Identity($user->id, $user->group->name, [
            'username' => $user->username,
            'groupType' => $user->group->type,
        ]);
    }
}