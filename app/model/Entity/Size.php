<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 */
class Size extends Base
{
    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $name;

}
