<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 */
class Service extends Base
{
    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\ManyToMany(targetEntity="Booking", mappedBy="services")
     * @ORM\JoinColumn(nullable=false)
     * @var Order
     */
    protected $bookings = [];

    /**
     * @ORM\ManyToMany(targetEntity="Order", mappedBy="services")
     * @ORM\JoinColumn(nullable=false)
     * @var Order
     */
    protected $orders = [];

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $description;

    /**
     * @ORM\Column(type="string")
     */
    protected $use;

    /**
     * @param Order $order
     */
    public function addOrder($order)
    {
        if (!$this->orders->contains($order)) {
            return;
        }
        $this->orders->add($order);
        $order->addService($this);
    }

    /**
     * @param Order $order
     */
    public function removeOrder($order)
    {
        if (!$this->orders->contains($order)) {
            return;
        }
        $this->orders->removeElement($order);
        $order->removeService($this);
    }

}