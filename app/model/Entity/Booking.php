<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Nette\Utils\DateTime;
use Tracy\Debugger;

/**
 * @ORM\Entity
 */
class Booking extends Base
{
    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $booking_number;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     * @var Customer
     */
    protected $customer;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     * @var User
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(nullable=false)
     * @var Shop
     */
    protected $shop;

    /**
     * @ORM\ManyToOne(targetEntity="Size")
     * @ORM\JoinColumn(nullable=false)
     * @var Size
     */
    protected $size;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    protected $date;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    protected $startDatetime;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $spz;

    /**
     * @ORM\Column(type="float")
     * @var float
     */
    protected $price;


    /**
     * @ORM\ManyToOne(targetEntity="Discount")
     * @ORM\JoinColumn(nullable=false)
     * @var Discount
     */
    protected $discount;

    /**
     * @ORM\ManyToMany(targetEntity="Service", inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     * @var Service
     */
    protected $services = [];


    public function __construct()
    {
        $this->startDatetime = new DateTime();
        $this->date = new DateTime('1970-01-02 00:00:00');
    }

    /**
     * @param Service $service
     */
    public function addService(Service $service)
    {
//        Debugger::barDump($service, 'uz tam je');
//        if (!$this->services->contains($service)) {
//            return;
//        }
        Debugger::barDump($this->services, 'add pred');
        $this->services->add($service);
        Debugger::barDump($this->services, 'add po');

        $service->addOrder($this);
    }

    /**
     * @param Service $service
     */
    public function removeService(Service $service)
    {
        if (!$this->services->contains($service)) {
            return;
        }
        Debugger::barDump($this->services, 'rem pred');
        $this->services->removeElement($service);
        Debugger::barDump($this->services, 'rem po');

        $service->removeOrder($this);
    }
}