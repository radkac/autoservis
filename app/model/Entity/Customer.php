<?php

namespace App\Model\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 */
class Customer extends Base
{
    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="customer")
     * @var Booking[]
     */
    protected $bookings = [];

    /**
     * @ORM\OneToMany(targetEntity="Order", mappedBy="customer")
     * @var Order[]
     */
    protected $orders = [];

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $phone;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $ico;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $address;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    protected $active = true;
}

