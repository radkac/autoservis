<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 */
class History extends Base
{
    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\ManyToOne(targetEntity="Order")
     * @ORM\JoinColumn(nullable=false)
     * @var Order
     */
    protected $order;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    protected $datetime;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $description;



}