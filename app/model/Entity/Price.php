<?php

namespace App\Model\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 */
class Price extends Base
{
    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(nullable=false)
     * @var Shop
     */
    protected $shop;

    /**
     * @ORM\ManyToOne(targetEntity="Service")
     * @ORM\JoinColumn(nullable=false)
     * @var Service
     */
    protected $service;

    /**
     * @ORM\ManyToOne(targetEntity="Size")
     * @ORM\JoinColumn(nullable=false)
     * @var Size
     */
    protected $size;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $value;

}