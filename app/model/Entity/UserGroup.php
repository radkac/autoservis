<?php

namespace App\Model\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 */
class UserGroup extends Base
{
    const GROUP_USER = 1;
    const GROUP_ADMIN = 2;
    const GROUP_FORMER_EMPLOYEE = 3;

    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="group")
     * @var User[]
     */
    protected $users = [];

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    protected $type = self::GROUP_USER;
}