<?php


namespace App\Model\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Nette\Utils\DateTime;


/**
 * @ORM\Entity
 * @property string $name
 * @property UserGroup $group
 */
class User extends Base
{
    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="user")
     * @var Booking[]
     */
    protected $bookings = [];

    /**
     * @ORM\OneToMany(targetEntity="Order", mappedBy="user")
     * @var Order[]
     */
    protected $orders = [];

    /**
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     * @var Shop
     */
    protected $shop;

    /**
     * @ORM\ManyToOne(targetEntity="UserGroup", inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     * @var UserGroup
     */
    protected $group;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $username;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $password;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    protected $lastLogin;
}