<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Nette\Utils\DateTime;
use Tracy\Debugger;

/**
 * @ORM\Entity
 * @ORM\Table(name="web_order")
 */
class Order extends Base
{
    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $order_number;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     * @var Customer
     */
    protected $customer;

    /**
     * @ORM\ManyToOne(targetEntity="Car")
     * @ORM\JoinColumn(nullable=false)
     * @var Car
     */
    protected $car;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     * @var User
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(nullable=false)
     * @var Shop
     */
    protected $shop;

    /**
     * @ORM\ManyToOne(targetEntity="Size")
     * @ORM\JoinColumn(nullable=false)
     * @var Size
     */
    protected $size;

    /**
     * @ORM\ManyToOne(targetEntity="Payment")
     * @ORM\JoinColumn(nullable=false)
     * @var Payment
     */
    protected $payment;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    protected $startDatetime;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    protected $endDatetime;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $spz;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    protected $inBox = false;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    protected $washed = false;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    protected $paid = false;

    /**
     * @ORM\Column(type="float")
     * @var float
     */
    protected $price;

    /**
     * @ORM\Column(type="float")
     * @var float
     */
    protected $priceAfterDiscount;

    /**
     * @ORM\ManyToOne(targetEntity="Discount")
     * @ORM\JoinColumn(nullable=false)
     * @var Discount
     */
    protected $discount;

    /**
     * @ORM\ManyToMany(targetEntity="Service", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     * @var Service
     */
    protected $services = [];
    
    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    protected $finished = false;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    protected $active = true;

    public function __construct()
    {
        $this->startDatetime = new DateTime();
        $this->endDatetime = new DateTime('1970-01-02 00:00:00');
    }

    /**
     * @param Service $service
     */
    public function addService(Service $service)
    {
//        Debugger::barDump($service, 'uz tam je');
//        if (!$this->services->contains($service)) {
//            return;
//        }
        Debugger::barDump($this->services, 'add pred');
        $this->services->add($service);
        Debugger::barDump($this->services, 'add po');

        $service->addOrder($this);
    }

    /**
     * @param Service $service
     */
    public function removeService(Service $service)
    {
        if (!$this->services->contains($service)) {
            return;
        }
        Debugger::barDump($this->services, 'rem pred');
        $this->services->removeElement($service);
        Debugger::barDump($this->services, 'rem po');

        $service->removeOrder($this);
    }
}