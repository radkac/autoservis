<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 */
class Shop extends Base
{
    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="shop")
     * @var User[]
     */
    protected $users = [];
    
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $address;
}