<?php

namespace App\Model;

use App\Model\Entity\UserGroup;
use Kdyby\Doctrine\EntityManager;

class UserGroupModel extends BaseModel
{
    private $entityManager;

    /**
     * CarModel constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @return array
     */
    public function getGroups()
    {
        return $this->entityManager->getRepository(UserGroup::class)->findPairs('name');
    }

    /**
     * @param $groupId
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getGroupById($groupId)
    {
        return $this->entityManager->find(UserGroup::class, $groupId);
    }


    /**
     * @return array
     */
    public function getGroupUsers()
    {
        return $this->entityManager->getRepository(UserGroup::class)->findAll();

    }
}