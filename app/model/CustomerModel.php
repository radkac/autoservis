<?php

namespace App\Model;


use App\Model\Entity\Customer;
use App\Model\Entity\Order;
use DateTime;
use Kdyby\Doctrine\EntityManager;
use Tracy\Debugger;


class CustomerModel extends BaseModel
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get all Customer from table Customer
     * 
     * @return array|\Doctrine\ORM\Query
     */
    public function getCustomers()
    {
        return $this->entityManager->getRepository(Customer::class)->findPairs('phone');
    }

    public function getAllCustomers()
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('c')
            ->from(Customer::class, 'c')
            ->where('c.active = :active')
            ->setParameter('active', 1);

        return $query->getQuery();
    }
    
    public function getCustomerName($fname)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('c')
            ->from(Customer::class, 'c')
            ->where('c.fname LIKE :fname')
            ->setParameter('fname', $fname);
        
        return $query->getQuery();
    }

    public function getCustomerByFirstLastName($fname, $lname, $ico)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('c')
            ->from(Customer::class, 'c')
            ->where('c.fname LIKE :fname')
            ->andWhere('c.lname LIKE :lname')
            ->andWhere('c.ico LIKE :ico')
            ->setParameters(['fname' => $fname, 'lname' => $lname, 'ico' => $ico]);

        return $query->getQuery();
    }

    public function getCustomerByPhone($phone, $ico)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('c')
            ->from(Customer::class, 'c')
            ->where('c.phone LIKE :phone')
            ->andWhere('c.ico LIKE :ico')
            ->setParameters(['phone' => $phone, 'ico' => $ico]);

        return $query->getQuery();
    }

    public function getCustomersByPhone($phone)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('c')
            ->from(Customer::class, 'c')
            ->where('c.phone LIKE :phone')
            ->setParameter('phone', "%$phone%")
            ->setMaxResults(10);

        return $query->getQuery();
    }

    
    public function getCustomerById($customerId)
    {
        return $this->entityManager->find(Customer::class, $customerId);
    }

    public function getCustomersByLastName($lastName)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('c')
            ->from(Customer::class, 'c')
            ->where('c.lname LIKE :lname')
            ->setParameter('lname', "%$lastName%")
            ->setMaxResults(10);

        return $query->getQuery();
    }

    /**
     * @param $customerId
     * @return \Doctrine\ORM\Query
     */
    public function getCloseOrders($customerId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('o')
            ->from(Order::class, 'o')
            ->where('o.customer = :customerId')
            ->andWhere('o.inBox = :done')
            ->andWhere('o.paid = :done')
            ->andWhere('o.washed = :done')
            ->andWhere('o.active = :yes')
            ->setParameters(['customerId'=>$customerId, 'done'=>'1', 'yes'=>'1']);

        return $query->getQuery();
    }

    /**
     * @param $customerId
     * @return \Doctrine\ORM\Query
     */
    public function getUncloseOrders($customerId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('o')
            ->from(Order::class, 'o')
            ->where('o.customer = :customerId')
            ->andWhere('o.inBox = :done OR o.paid = :done OR o.washed = :done')
            ->andWhere('o.active = :yes')
            ->setParameters(['customerId'=>$customerId, 'done'=>'0', 'yes'=>'1']);

        return $query->getQuery();
    }
}