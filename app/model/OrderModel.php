<?php

namespace App\Model;


use App\Model\Entity\Order;
use Kdyby\Doctrine\EntityManager;
use Nette\Utils\DateTime;
use Tracy\Debugger;


class OrderModel extends BaseModel
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get all Books from table Order
     *
     * @return \Doctrine\ORM\Query
     */
    public function getOrders()
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('o');
        $query->from(Order::class, 'o');
        return $query->getQuery();
    }
    /**
     * @param $orderId
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getOrderById($orderId)
    {
        return $this->entityManager->find(Order::class, $orderId);
    }
    
    public function getOrdersByUser($userId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('o')
            ->from(Order::class, 'o')
            ->where('o.user = :userId')
            ->andWhere('o.active = :yes')
            ->setParameters(['userId'=>$userId, 'yes'=>'1']);

        return $query->getQuery();
    }

    public function getOrderByEndDate($date)
    {
        $formatedDate = $date->format('Y-m-d');
        $query = $this->entityManager->createQueryBuilder();
        $query->select('o')
            ->from(Order::class, 'o')
            ->where('substring(o.endDatetime, 1, 10) LIKE :date')
            ->distinct(true)
            ->andWhere('o.active = :yes')
            ->setParameters(['date'=>$formatedDate, 'yes'=>'1']);
        
        return $query->getQuery();
    }

    public function getOrderByStartDate($date)
    {
        $formatedDate = $date->format('Y-m-d');
        $query = $this->entityManager->createQueryBuilder();
        $query->select('o')
            ->from(Order::class, 'o')
            ->where('substring(o.startDatetime, 1, 10) LIKE :date')
            ->andWhere('o.active = :yes')
            ->distinct(true)
            ->setParameters(['date'=>$formatedDate, 'yes'=>'1']);

        return $query->getQuery();
    }

    /**
     * @param $userId
     * @return \Doctrine\ORM\Query
     */
    public function getCloseOrders($userId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('o')
            ->from(Order::class, 'o')
            ->where('o.user = :userId')
            ->andWhere('o.finished = :done')
            ->andWhere('o.active = :yes')
            ->setParameters(['userId'=>$userId, 'done'=>'1', 'yes'=>'1'])
            ->orderBy('o.endDatetime', 'DESC');

        return $query->getQuery();
    }

    /**
     * @param $userId
     * @return \Doctrine\ORM\Query
     */
    public function getUncloseOrders($userId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('o')
            ->from(Order::class, 'o')
            ->where('o.user = :userId')
            ->andWhere('o.finished = :done')
            ->andWhere('o.active = :yes')
            ->setParameters(['userId'=>$userId, 'done'=>'0', 'yes'=>'1'])
            ->orderBy('o.startDatetime', 'DESC');
        
        return $query->getQuery();
    }

    /**
     * @param $shopId
     * @return \Doctrine\ORM\Query
     */
    public function getShopCloseOrders($shopId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('o')
            ->from(Order::class, 'o')
            ->where('o.shop = :shopId')
            ->andWhere('o.finished = :done')
            ->andWhere('o.active = :yes')
            ->setParameters(['shopId'=>$shopId, 'done'=>'1', 'yes'=>'1'])
            ->orderBy('o.endDatetime', 'DESC');

        return $query->getQuery();
    }

    /**
     * @param $shopId
     * @return \Doctrine\ORM\Query
     */
    public function getShopUncloseOrders($shopId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('o')
            ->from(Order::class, 'o')
            ->where('o.shop = :shopId')
            ->andWhere('o.finished = :done')
            ->andWhere('o.active = :yes')
            ->setParameters(['shopId'=>$shopId, 'done'=>'0', 'yes'=>'1'])
            ->orderBy('o.startDatetime', 'DESC');

        return $query->getQuery();
    }
    
    public function getSalesCloseOrders($shopId)
    {
	    $query = $this->entityManager->createQueryBuilder();
	    $query->select('o')
			    ->from(Order::class, 'o')
			    ->where('o.finished = :done')
			    ->andWhere('o.shop = :shopId')
			    ->andWhere('o.active = :yes')
			    ->setParameters(['done'=>'1', 'shopId' => $shopId, 'yes'=>'1'])
			    ->orderBy('o.endDatetime', 'DESC');
	
	    return $query->getQuery();
    }

    public function getSales($shopId, $dateFrom, $dateTo)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('o')
            ->from(Order::class, 'o')
            ->where('o.finished = :done')
            ->andWhere('o.shop = :shopId')
            ->andWhere('o.active = :yes')
            ->andWhere('o.endDatetime BETWEEN :dateFrom AND :dateTo')
            ->setParameters(['done'=>'1', 'shopId' => $shopId, 'yes'=>'1', 'dateFrom'=>$dateFrom, 'dateTo'=>$dateTo])
            ->orderBy('o.endDatetime', 'DESC');

        return $query->getQuery();
    }

    public function getSalesByDate($shopId, $date, $month, $orders)
    {
        // get all orders
        $todayOrders = [];
        $daySalesInfo = [];
        if($date != null) {
            $date = new DateTime($date);

            $sales = $this->getSales($shopId,$date->format('d.m.Y') ." 00:00:00",
                $date->format('d.m.Y') ." 23:59:59")->getResult();
            $dateFormat = $date->format('d.m.Y');

            foreach ($orders as $order) {
                $orderDate = $order->endDatetime->format('d.m.Y');
                if ($dateFormat == $orderDate) {
                    $todayOrders[$order->payment->id][$order->id] = $order;
                }
            }

            // separe different payment methods
            $invoice = array_slice($todayOrders, 0, 1);
            $cash = array_slice($todayOrders, 1, 2);

            $daySales = $this->getSalesValue($todayOrders);

            $invoiceSales = 0;
            $cashSales = 0;

            $daySalesInfo['invoiceCount'] = 0;
            $daySalesInfo['cashCount'] = 0;

            if ($invoice != NULL) {
                foreach ($invoice[0] as $invoiceOrder) {
                    $invoiceSales += $invoiceOrder->priceAfterDiscount;
                }
                $daySalesInfo['invoiceCount'] = count(array_values($invoice[0]));
            }

            if ($cash != NULL) {
                foreach ($cash[0] as $cashOrder) {
                    $cashSales += $cashOrder->priceAfterDiscount;
                }
                $daySalesInfo['cashCount'] = count(array_values($cash[0]));
            }

            // fill sales info
            $daySalesInfo['sales'] = $daySales[0];
            $daySalesInfo['discountCount'] = $daySales[1];
            $daySalesInfo['invoiceSales'] = $invoiceSales;
            $daySalesInfo['cashSales'] = $cashSales;

//            $this->template->date = $date; // poslat do return
        }

        // MONTH SALES

        $monthSalesInfo = [];
        $monthOrders = [];

        if($month != null) {
            $month = new DateTime($month);
            $monthFormat = $month->format('m.Y');

            foreach ($orders as $order) {
                $orderDate = $order->endDatetime->format('m.Y');
                if ($monthFormat == $orderDate) {
                    $monthOrders[$order->payment->id][$order->id] = $order;
                }
            }

            $invoice = 0;
            $cash = 0;

            if (array_key_exists('1',$monthOrders))
            {
                $cash = $monthOrders[1];
            }

            if (array_key_exists('2',$monthOrders))
            {
                $invoice = $monthOrders[2];
            }

            $monthSales = $this->getSalesValue($monthOrders);
            $invoiceSales = 0;
            $cashSales = 0;

            $monthSalesInfo['invoiceCount'] = 0;
            $monthSalesInfo['cashCount'] = 0;

            if ($invoice != NULL) {
                foreach ($invoice as $invoiceOrder) {
                    $invoiceSales += $invoiceOrder->priceAfterDiscount;
                }
                $monthSalesInfo['invoiceCount'] = count(array_values($invoice));
            }

            if ($cash != NULL) {
                foreach ($cash as $cashOrder) {
                    $cashSales += $cashOrder->priceAfterDiscount;
                }
                $monthSalesInfo['cashCount'] = count(array_values($cash));
            }

            // fill month sales info
            $monthSalesInfo['sales'] = $monthSales[0];
            $monthSalesInfo['discountCount'] = $monthSales[1];
            $monthSalesInfo['invoiceSales'] = $invoiceSales;
            $monthSalesInfo['cashSales'] = $cashSales;
        }

        if($month != null)
        {
            $month = $month->format('F Y');
        }

        if($date != null)
        {
            $date = $date->format('d.m.Y');
        }

        return array (
            'date' => $date,
            'month' => $month,
            'todayOrders' => $todayOrders,
            'monthOrders' => $monthOrders,
            'daySalesInfo' => $daySalesInfo,
            'monthSalesInfo' => $monthSalesInfo
        );
    }

    public function getSalesValue($orders)
    {
        $sales = 0;
        $discountCount = 0;
        foreach($orders as $order)
        {
            foreach($order as $one) {
                $sales += $one->priceAfterDiscount;
                if($one->discount->id != 1)
                {
                    $discountCount += 1;
                }
            }
        }

        return array($sales, $discountCount);
    }
}