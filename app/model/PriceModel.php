<?php

namespace App\Model;

use App\Model\Entity\Price;
use Kdyby\Doctrine\EntityManager;

class PriceModel extends BaseModel
{
    private $entityManager;

    /**
     * ServiceModel constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getPrice($serviceId, $sizeId, $shopId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('p.value')
            ->from(Price::class, 'p')
            ->where('p.service = :serviceId')
            ->andWhere('p.size = :sizeId')
            ->andWhere('p.shop = :shopId')
            ->setParameters(['serviceId'=>$serviceId, 'sizeId'=>$sizeId, 'shopId'=>$shopId]);

        return $query->getQuery();
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getPrices()
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('p')
            ->from(Price::class, 'p');

        return $query->getQuery();
    }

    /**
     * @param $priceId
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getPriceById($priceId)
    {
        return $this->entityManager->find(Price::class, $priceId);
    }
}