<?php

namespace App\Model;

use App\Model\Entity\Size;
use Kdyby\Doctrine\EntityManager;

class SizeModel extends BaseModel
{
    private $entityManager;

    /**
     * ServiceModel constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getSizes()
    {
        return $this->entityManager->getRepository(Size::class)->findPairs('name');
    }

    /**
     * @param $sizeId
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getPriceById($sizeId)
    {
        return $this->entityManager->find(Size::class, $sizeId);
    }
}