<?php

namespace App\Model;

use App\Model\Entity\Payment;
use Kdyby\Doctrine\EntityManager;

class PaymentModel extends BaseModel
{
    private $entityManager;

    /**
     * ServiceModel constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getPayments()
    {
        return $this->entityManager->getRepository(Payment::class)->findPairs('name');
    }
}