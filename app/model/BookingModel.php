<?php

namespace App\Model;


use App\Model\Entity\Booking;
use Kdyby\Doctrine\EntityManager;
use Tracy\Debugger;


class BookingModel extends BaseModel
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get all Orders from table Order
     *
     * @return \Doctrine\ORM\Query
     */
    public function getBookings()
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('b');
        $query->from(Booking::class, 'b');
        return $query->getQuery();
    }
    /**
     * @param $bookingId
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getBookingById($bookingId)
    {
        return $this->entityManager->find(Booking::class, $bookingId);
    }

    public function getBookingsByShop($shopId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('b')
            ->from(Booking::class, 'b')
            ->where('b.shop = :shopId')
            ->setParameters(['shopId'=>$shopId]);

        return $query->getQuery();
    }

    public function getBookingsByCustomer($customerId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('b')
            ->from(Booking::class, 'b')
            ->where('b.customer = :customerId')
            ->setParameters(['customerId'=>$customerId]);

        return $query->getQuery();
    }

    public function getBookingByDate($date)
    {
        $formatedDate = $date->format('Y-m-d');
        $query = $this->entityManager->createQueryBuilder();
        $query->select('b')
            ->from(Booking::class, 'b')
            ->where('substring(b.date, 1, 10) LIKE :date')
            ->distinct(true)
            ->setParameters(['date'=>$formatedDate]);

        return $query->getQuery();
    }

    public function getOrderByStartDate($date)
    {
        $formatedDate = $date->format('Y-m-d');
        $query = $this->entityManager->createQueryBuilder();
        $query->select('b')
            ->from(Booking::class, 'b')
            ->where('substring(b.startDatetime, 1, 10) LIKE :date')
            ->andWhere('b.active = :yes')
            ->distinct(true)
            ->setParameters(['date'=>$formatedDate, 'yes'=>'1']);

        return $query->getQuery();
    }

    /**
     * @param $shopId
     * @return \Doctrine\ORM\Query
     */
    public function getShopBookings($shopId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('b')
            ->from(Booking::class, 'b')
            ->where('b.shop = :shopId')
            ->setParameter('shopId', $shopId)
            ->orderBy('b.startDatetime', 'DESC');

        return $query->getQuery();
    }

    /**
     * @param $userId
     * @return \Doctrine\ORM\Query
     */
    public function getUserBookings($userId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('b')
            ->from(Booking::class, 'b')
            ->where('b.user = :userId')
            ->setParameter('userId', $userId)
            ->orderBy('b.date', 'DESC');

        return $query->getQuery();
    }
}