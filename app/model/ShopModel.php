<?php

namespace App\Model;

use App\Model\Entity\Shop;
use Kdyby\Doctrine\EntityManager;

class ShopModel extends BaseModel
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get all Shops from table Shop
     *
     * @return \Doctrine\ORM\Query
     */
    public function getShopsById()
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('s');
        $query->from(Shop::class, 's');
        return $query->getQuery();
    }

    /**
     * @return array
     */
    public function getShops()
    {
        return $this->entityManager->getRepository(Shop::class)->findPairs('name');
    }

    /**
     * @param $shopId
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getShopById($shopId)
    {
        return $this->entityManager->find(Shop::class, $shopId);
    }
}