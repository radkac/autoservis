<?php

namespace App\Model;

use App\Model\Entity\Car;
use Kdyby\Doctrine\EntityManager;

class CarModel extends BaseModel
{
    private $entityManager;

    /**
     * CarModel constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @return array
     */
    public function getCars()
    {
        return $this->entityManager->getRepository(Car::class)->findPairs('name');
    }
    /**
     * @param $carId
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getCarById($carId)
    {
        return $this->entityManager->find(Car::class, $carId);
    }
}