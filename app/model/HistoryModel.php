<?php

namespace App\Model;


use App\Model\Entity\Customer;
use App\Model\Entity\Discount;
use App\Model\Entity\History;
use App\Model\Entity\Order;
use DateTime;
use Kdyby\Doctrine\EntityManager;
use Tracy\Debugger;


class HistoryModel extends BaseModel
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get all Histories from table History
     *
     * @return \Doctrine\ORM\Query
     */
    public function getHistories()
    {
        return $this->entityManager->getRepository(History::class)->findPairs('datetime');
    }

    public function getHistoriesByOrderId($orderId)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('h')
            ->from(History::class, 'h')
            ->where('h.order = :orderId')
            ->setParameter('orderId', $orderId)
            ->orderBy('h.datetime', 'DESC');

        return $query->getQuery();
    }


    public function getHistoryDateTime($datetime)
    {
        $query = $this->entityManager->createQueryBuilder();
        $query->select('h')
            ->from(History::class, 'h')
            ->where('h.datetime LIKE :datetime')
            ->setParameter('datetime', $datetime);

        return $query->getQuery();
    }

    public function getHistoryById($historyId)
    {
        return $this->entityManager->find(History::class, $historyId);
    }
}