<?php

namespace App\FrontModule\Presenters;

use App\Components\SignInForm\ISignInForm;
use App\Components\SignInForm\SignInForm;
use App\Model\Entity\User;
use App\Model\Entity\UserGroup;
use Nette\Application\UI\Form;


class SignPresenter extends BasePresenter
{
    /**
     * @inject
     * @var \App\Model\PasswordAuthenticator
     */
    public $passwordAuthenticator;

    /**
     * @inject
     * @var \App\Model\UserModel
     */
    public $userModel;

    /**
     * @var User
     */
    public $employee;

    public function actionDefault()
    {
        $this->redirect(':Front:Sign:in');
    }

    public function actionIn()
    {
        if($this->user->isLoggedIn())
        {
            if ($this->user->identity->groupType == UserGroup::GROUP_ADMIN) {
                $this->redirect(':Admin:Users:default');
            } else {
                $this->redirect(':Employee:Homepage:default');
            }
        }
        if(!$this->user->guestRole) {
            if ($this->user->identity->groupType == UserGroup::GROUP_ADMIN) {
                $this->redirect(':Admin:Users:default');
            } else {
                $this->redirect(':Employee:Homepage:default');
            }
        }
    }

    public function actionOut()
    {
        if ($this->user->isLoggedIn()) {
            $this->user->logout();
            $this->flashMessage('Odhlásenie prebehlo úspešne.');
            $this->redirect(':Front:Sign:in');
        } else {
            $this->flashMessage('Odhlásenie nebolo potrebné.');
            $this->redirect(':Front:Sign:in');
        }
    }

    public function createComponentSignInForm()
    {
        /**
         * @var SignInForm
         */
        $form = $this->context->getByType(ISignInForm::class)->create();
        $form->addSuccessCallback($this->signInFormSucceeded);
        return $form;
    }

    public function signInFormSucceeded(Form $form, $values)
    {
        if ($this->user->identity->groupType == UserGroup::GROUP_ADMIN) {
            $this->redirect(':Admin:Users:default');
        } else {
            $this->redirect(':Employee:Homepage:default');
        }

        $this->flashMessage('Vitajte!');
    }
}