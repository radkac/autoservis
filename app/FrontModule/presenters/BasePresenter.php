<?php

namespace App\FrontModule\Presenters;

use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter
{
    public function beforeRender()
    {
        $filePath = $this->context->parameters['filePath'];
        $debug = !$this->context->parameters['productionMode'];
        
        $this->template->debugMode = $debug;
        $this->template->filePath = $filePath;

    }
}