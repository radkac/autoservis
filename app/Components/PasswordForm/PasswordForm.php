<?php

namespace App\Components\PasswordForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\UserModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;

class PasswordForm extends BaseForm
{
    /**
     * @var array
     */
    private $onSuccess = [];

    /**
     * @var UserModel
     */
    private $userModel;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var boolean
     */
    private $admin = FALSE;


    public function __construct(EntityManager $entityManager, UserModel $userModel)
    {
        $this->entityManager = $entityManager;
        $this->userModel = $userModel;
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/templates/passwordForm.latte');
    }

    /**
     * @param $callback
     */
    public function addSuccessCallback($callback)
    {
        $this->onSuccess[] = $callback;
    }

    public function enableAdmin()
    {
        $this->admin = TRUE;
    }

    protected function createComponentPasswordForm()
    {
        $form = new Form();
        $form->setRenderer(new FormRenderer);

        $form->addPassword('passwordOld', 'Staré heslo')
            ->setRequired('Musíte zadať aktuálne heslo.');
        $form->addPassword('password', 'Nové heslo')
            ->setRequired('Musíte zadať heslo.')
            ->setOption('description', 'Heslo musí mať aspoň 8 znakov.')
            ->addRule(Form::MIN_LENGTH, 'Heslo musí mať aspoň %d znakov.', 8);
        $form->addPassword('passwordCheck', 'Overenie hesla')
            ->setRequired('Musíte zadať nové heslo.')
            ->addConditionOn($form["password"], Form::FILLED)
            ->addRule(Form::EQUAL, "Heslá se musia zhodovať !", $form["password"]);

        $form->addSubmit('add', 'Uložiť');

        $form->onValidate[] = [$this, 'onValidate'];
        $form->onSuccess[] = [$this, 'passwordFormSucceeded'];

        foreach ($this->onSuccess as $callback) {
            $form->onSuccess[] = $callback;
        }

        return $form;
    }

    public function onValidate(Form $form, $values)
    {
        if (!$this->admin) {
            if (!(Passwords::verify($values->passwordOld, $this->defaultEntity->password))) {
                $form['passwordOld']->addError('Aktuálne heslo nie je správne.');
            }
        }
    }

    public function passwordFormSucceeded(Form $form, $values)
    {
        $this->defaultEntity->password = Passwords::hash($values->password);

        $this->entityManager->persist($this->defaultEntity);


        $this->entityManager->flush();
    }
}

interface IPasswordForm
{
    /**
     * @return PasswordForm
     */
    public function create();
}
