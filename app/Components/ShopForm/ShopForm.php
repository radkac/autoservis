<?php

namespace App\Components\ShopForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\Entity\Shop;
use App\Model\ShopModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;

class ShopForm extends BaseForm
{
    /**
     * @var array
     */
    private $onSuccess = [];

    /**
     * @var ShopModel
     */
    private $shopModel;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager, ShopModel $priceModel)
    {
        $this->entityManager = $entityManager;
        $this->shopModel = $priceModel;
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/templates/shopForm.latte');
    }

    public function addSuccessCallback($callback)
    {
        $this->onSuccess[] = $callback;
    }

    protected function createComponentShopForm()
    {
        $form = new Form();
        $form->addHidden('id');
        $form->setRenderer(new FormRenderer);
        $form->addText('name', 'Názov predajne')
            ->setRequired('Musíte zadať názov predajne');
        $form->addText('address', 'Adresa')
            ->setRequired('Musíte zadať adresu predajne.');

        if ($this->defaultEntity) {
            $this->defaultEntity = $this->shopModel->getShopById($this->defaultEntity->getId());

            $shopData = [
                'id' => $this->defaultEntity->id,
                'name' => $this->defaultEntity->name,
                'address' => $this->defaultEntity->address,
            ];
            $form->setDefaults($shopData);
        }

        $form->addSubmit('add', 'Uložiť');
        $form->onSuccess[] = [$this, 'shopFormSucceeded'];

        foreach ($this->onSuccess as $callback) {
            $form->onSuccess[] = $callback;
        }

        return $form;
    }

    public function shopFormSucceeded(Form $form, $values)
    {
        if ($this->defaultEntity) {
            $this->defaultEntity->name = $values->name;
            $this->defaultEntity->address = $values->address;

            $this->entityManager->persist($this->defaultEntity);
        } else {
            $shop = new Shop();
            $shop->name = $values->name;
            $shop->address = $values->address;

            $this->entityManager->persist($shop);
        }
        $this->entityManager->flush();
    }
}

interface IShopForm
{
    /**
     * @return ShopForm
     */
    public function create();
}