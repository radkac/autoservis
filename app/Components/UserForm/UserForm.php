<?php

namespace App\Components\UserForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\Entity\Shop;
use App\Model\Entity\User;
use App\Model\Entity\UserGroup;
use App\Model\ShopModel;
use App\Model\UserGroupModel;
use App\Model\UserModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;
use Nette\Utils\DateTime;

class UserForm extends BaseForm
{
    /**
     * @var array
     */
    private $onSuccess = [];

    /**
     * @var UserModel
     */
    private $userModel;

    /**
     * @var UserGroupModel
     */
    private $groupModel;

    /**
     * @var ShopModel
     */
    private $shopModel;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * CustomerForm constructor.
     * @param EntityManager $entityManager
     * @param UserModel $userModel
     * @param UserGroupModel $groupModel
     * @param ShopModel $shopModel
     */
    public function __construct(EntityManager $entityManager, UserModel $userModel, UserGroupModel $groupModel, ShopModel $shopModel)
    {
        $this->entityManager = $entityManager;
        $this->userModel = $userModel;
        $this->groupModel = $groupModel;
        $this->shopModel = $shopModel;
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/templates/userForm.latte');
    }

    /**
     * @param $callback
     */
    public function addSuccessCallback($callback)
    {
        $this->onSuccess[] = $callback;
    }

    /**
     * @return Form
     */
    protected function createComponentUserForm()
    {
        $groups = $this->groupModel->getGroups();
        $shops = $this->shopModel->getShops();

        $form = new Form();
        $form->addHidden('id');
        $form->setRenderer(new FormRenderer);
        $form->addText('name', 'Meno a priezvisko')
            ->setRequired('Musíte zadať meno a priezvisko.')
            ->addRule(Form::MIN_LENGTH, 'Meno a priezvisko musí mať aspoň %d znakov.', 5);
        $form->addText('username', 'Prihlasovacie meno')
            ->setRequired('Musíte zadať prihlasovacie meno.');

        if (!$this->defaultEntity) {
            $form->addPassword('password', 'Heslo')
                ->setRequired('Musíte zadať heslo.')
                ->setOption('description', 'Heslo musí mať aspoň 8 znakov.')
                ->addRule(Form::MIN_LENGTH, 'Heslo musí mať aspoň %d znakov.', 8);
            $form->addPassword('passwordCheck', 'Heslo znovu')
                ->setRequired('Musíte zadať heslo.')
                ->addConditionOn($form["password"], Form::FILLED)
                ->addRule(Form::EQUAL, "Heslá se musia zhodovať!", $form["password"]);
        }
        $form->addSelect('shopId', 'Prevádzka', $shops)
            ->setPrompt('Zvoľte prevádzku.');
        $form->addSelect('groupId', 'Skupina používateľa', $groups)
            ->setPrompt('Zvoľte skupinu pre používateľa.');


        if ($this->defaultEntity) {
            $this->defaultEntity = $this->userModel->getUserById($this->defaultEntity->getId());

            $userData = [
                'id' => $this->defaultEntity->getId(),
                'name' => $this->defaultEntity->name,
                'username' => $this->defaultEntity->username,
                'password' => $this->defaultEntity->password,
                'shopId' => $this->defaultEntity->shop->id,
                'groupId' => $this->defaultEntity->group->id,
            ];
            $form->setDefaults($userData);
        }

        $form->addSubmit('add', 'Uložiť');

        $form->onValidate[] = [$this, 'onValidate'];
        $form->onSuccess[] = [$this, 'userFormSucceeded'];

        foreach ($this->onSuccess as $callback) {
            $form->onSuccess[] = $callback;
        }

        return $form;
    }

    public function onValidate($form, $values)
    {
        if (!$this->defaultEntity) {
            if ($this->userModel->getUserByUsername($values->username)) {
                $form['username']->addError('Používateľ so zadaným prihlasovacím menom už existuje');
            }
        }
    }

    public function userFormSucceeded(Form $form, $values)
    {
        if ($this->defaultEntity) {
            $this->defaultEntity->name = $values->name;
            $this->defaultEntity->username = $values->username;
            $this->defaultEntity->shop = $this->entityManager->getRepository(Shop::class)->findOneBy(['id' => $values->shopId]);
            $this->defaultEntity->group = $this->entityManager->getRepository(UserGroup::class)->findOneBy(['id' => $values->groupId]);

            $this->entityManager->persist($this->defaultEntity);
        } else {
            $user = new User();
            $user->shop = $this->entityManager->getRepository(Shop::class)->findOneBy(['id' => $values->shopId]);
            $user->group = $this->entityManager->getRepository(UserGroup::class)->findOneBy(['id' => $values->groupId]);
            $user->name = $values->name;
            $user->username = $values->username;
            $user->password = Passwords::hash($values->password);
            $user->lastLogin = new DateTime();

            $this->entityManager->persist($user);
        }
        $this->entityManager->flush();
    }
}

interface IUserForm
{
    /**
     * @return UserForm
     */
    public function create();
}