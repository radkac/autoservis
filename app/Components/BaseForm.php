<?php

namespace App\Components;

use Nette\Application\UI\Control;

abstract class BaseForm extends Control
{
    protected $defaultEntity;

    /**
     * @param $entity
     */
    public function setDefaultEntity($entity)
    {
        $this->defaultEntity = $entity;
    }

}