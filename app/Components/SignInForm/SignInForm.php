<?php

namespace App\Components\SignInForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\Entity\User;
use App\Model\ShopModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;

class SignInForm extends BaseForm
{
    /**
     * @var array
     */
    private $onSuccess = [];
    
    /**
     * @var ShopModel
     */
    private $shopModel;

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * SignInForm constructor.
     *
*@param EntityManager $entityManager
     * @param ShopModel $priceModel
     */
    public function __construct(EntityManager $entityManager, ShopModel $priceModel)
    {
        $this->entityManager = $entityManager;
        $this->shopModel = $priceModel;
    }

    /**
     * Render function
     */
    public function render()
    {
        $this->template->render(__DIR__ . '/templates/signInForm.latte');
    }

    /**
     * @param $callback
     */
    public function addSuccessCallback($callback)
    {
        $this->onSuccess[] = $callback;
    }

    /**
     * @return Form
     */
    protected function createComponentSignInForm()
    {
        $form = new Form();
        $form->setRenderer(new FormRenderer);
        $form->addHidden('id');
        $form->addText('username', 'Meno: ')
            ->setRequired('Zadajte prihlasovacie meno.');
        $form->addPassword('password', 'Heslo: ')
            ->setRequired('Zadajte heslo.');

        $form->addSubmit('signIn', 'Prihlásiť');

        $form->onSuccess[] = [$this, 'signInFormSucceeded'];

        foreach ($this->onSuccess as $callback) {
            $form->onSuccess[] = $callback;
        }
        return $form;
    }

    /**
     * @param Form $form
     * @param $values
     */
    public function signInFormSucceeded(Form $form, $values)
    {
        try {
            $this->presenter->user->login($values->username, $values->password);
        } catch (AuthenticationException $e) {
            $form['password']->addError('Heslo nie je správne.');
        }
    }
}

interface ISignInForm
{
    /**
     * @return SignInForm
     */
    public function create();
}