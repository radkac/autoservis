<?php

namespace App\Components\DeleteForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\Entity\User;
use App\Model\Entity\UserGroup;
use App\Model\UserGroupModel;
use Doctrine\Common\Util\Debug;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;
use Tracy\Debugger;

class DeleteForm extends BaseForm
{
    /**
     * @var array
     */
    private $onSuccess = [];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var UserGroupModel
     */
    private $userGroupModel;

    /**
     * DeleteForm constructor.
     * @param EntityManager $entityManager
     * @param UserGroupModel $userGroupModel
     */
    public function __construct(EntityManager $entityManager, UserGroupModel $userGroupModel)
    {
        $this->entityManager = $entityManager;
        $this->userGroupModel = $userGroupModel;
    }

    /**
     * @param $entity
     */
    public function setDefaultEntity($entity)
    {
        $this->defaultEntity = $entity;
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/templates/deleteForm.latte');
    }

    /**
     * @param $callback
     */
    public function addSuccessCallback($callback)
    {
        $this->onSuccess[] = $callback;
    }

    /**
     * @return Form
     */
    protected function createComponentDeleteForm()
    {
        $form = new Form();
        $form->addHidden('id');
        $form->setRenderer(new FormRenderer);
        $form->addSubmit('cancel', 'Späť')
            ->setValidationScope(FALSE)
            ->onClick[] = array($this, 'cancelClicked');
        $form->addSubmit('yes', 'Vymazať');


        $form->onSuccess[] = [$this, 'deleteFormSucceeded'];

        foreach ($this->onSuccess as $callback) {
            $form->onSuccess[] = $callback;
        }

        return $form;
    }

    public function cancelClicked(SubmitButton $button)
    {
        $this->getPresenter()->redirect('Users:default');
    }

    public function deleteFormSucceeded(Form $form, $values)
    {
        if($this->defaultEntity) {
            $this->defaultEntity->group =  $this->entityManager->getRepository(UserGroup::class)->findOneBy(['id'=>3]);
            $this->entityManager->persist($this->defaultEntity);
        }
        $this->entityManager->flush();
    }
}

interface IDeleteForm
{
    /**
     * @return DeleteForm
     */
    public function create();
}