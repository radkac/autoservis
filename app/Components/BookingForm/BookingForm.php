<?php

namespace App\Components\BookingForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\CarModel;
use App\Model\CustomerModel;
use App\Model\DiscountModel;
use App\Model\Entity\Booking;
use App\Model\Entity\Car;
use App\Model\Entity\Customer;
use App\Model\Entity\Discount;
use App\Model\Entity\History;
use App\Model\Entity\Order;
use App\Model\Entity\OrderService;
use App\Model\Entity\Payment;
use App\Model\Entity\Service;
use App\Model\Entity\Size;
use App\Model\Entity\User;
use App\Model\HistoryModel;
use App\Model\OrderServiceModel;
use App\Model\PaymentModel;
use App\Model\PriceModel;
use App\Model\ServiceModel;
use App\Model\SizeModel;
use Doctrine\Common\Util\Debug;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;
use Tracy\Debugger;

class BookingForm extends BaseForm
{
    /**
     * @var array
     */
    private $onSuccess = [];

    /**
     * @var CarModel
     */
    private $carModel;

    /**
     * @var CustomerModel
     */
    private $customerModel;

    /**
     * @var ServiceModel
     */
    private $serviceModel;

    /**
     * @var PriceModel
     */
    private $priceModel;

    /**
     * @var SizeModel
     */
    private $sizeModel;

    /**
     * @var DiscountModel
     */
    private $discountModel;

    /**
     * @var PaymentModel
     */
    private $paymentModel;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var User
     */
    private $employee;

    /**
     * @var Customer
     */
    private $customer;


    /**
     * OrderForm constructor.
     * @param EntityManager $entityManager
     * @param CarModel $carModel
     * @param CustomerModel $customerModel
     * @param ServiceModel $serviceModel
     * @param SizeModel $sizeModel
     * @param PriceModel $priceModel
     * @param DiscountModel $discountModel
     * @param PaymentModel $paymentModel
     */
    public function __construct(EntityManager $entityManager, CarModel $carModel, CustomerModel $customerModel,
                                ServiceModel $serviceModel, SizeModel $sizeModel, PriceModel $priceModel,
                                DiscountModel $discountModel, PaymentModel $paymentModel)
    {
        $this->entityManager = $entityManager;
        $this->carModel = $carModel;
        $this->customerModel = $customerModel;
        $this->serviceModel = $serviceModel;
        $this->sizeModel = $sizeModel;
        $this->priceModel = $priceModel;
        $this->discountModel = $discountModel;
        $this->paymentModel = $paymentModel;
    }

    /**
     * @param $user
     */
    public function setEmployee($user)
    {
        $this->employee = $user;
        Debugger::barDump($this->employee);
    }

    /**
     * @param $customerId
     */
    public function setCustomer($customerId)
    {
        $this->customer = $this->customerModel->getCustomerById($customerId);
    }

    public function setServices($orderId)
    {
//        $this->orderServices = $this->orderServiceModel->getServices($orderId)->getResult();
//        Debugger::barDump($this->orderServices, 'orderServices');
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/templates/bookingForm.latte');
    }

    /**
     * @param $callback
     */
    public function addSuccessCallback($callback)
    {
        $this->onSuccess[] = $callback;
    }

    /**
     * @return Form
     */
    protected function createComponentBookingForm()
    {
        $services = $this->serviceModel->getServices();
        $sizes = $this->sizeModel->getSizes();
        $hours = [];

        $start = "8:00";
        $end = "18:30";

        $tStart = strtotime($start);
        $tEnd = strtotime($end);
        $tNow = $tStart;

        while($tNow <= $tEnd) {
            $hours[date("H:i", $tNow)] = date("H:i", $tNow);
            $tNow = strtotime('+30 minutes', $tNow);
        }

        unset($sizes[0]);
        $customersPhone = $this->customerModel->getAllCustomers()->getResult();
        $phones = [];
        foreach($customersPhone as $customer)
        {
            $phones[$customer->id] = $customer->phone;
        }

        $form = new Form();

        $form->setRenderer(new FormRenderer);
        $form->addHidden('id');
        $form->addText('date', 'Dátum')
            ->setRequired('Musíte zadať dátum.')
            ->setAttribute("id", "datepicker");
//            ->setAttribute("placeholder", "dd.mm.rrrr")
//            ->addRule($form::PATTERN, "Dátum musí byť vo formáte dd.mm.rrrr", "(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[012])\.(19|20)\d\d");
        $form->addSelect('time', 'Čas', $hours)
            ->setRequired('Musíte zadať čas.');
        $form->addText('spz', 'ŠPZ')
            ->setRequired('Musíte zadať ŠPZ.')
            ->addRule(Form::PATTERN, ' ŠPZ nesmie obsahovať malé písmená a píše sa bez pomlčky.', '^[A-Z]{2}(([\d]{3})[A-Z]{2}|[A-Z\d]{5})$')
            ->setOption('description', 'ŠPZ nesmie obsahovať malé písmená a píše sa bez pomlčky.');
        $form->addSelect('sizeId', 'Veľkosť auta', $sizes)
            ->setPrompt('Zvoľte veľkosť.')
            ->setRequired('Musíte zadať veľkosť auta.');
        $form->addSelect('serviceId1', 'Služba', $services)
            ->setPrompt('Zvoľte službu.')
            ->setRequired('Musíte zadať službu.');
        $form->addSelect('serviceId2', '+ Služba', $services)
            ->setPrompt('Zvoľte službu.');
        $form->addSelect('serviceId3', '+ Služba', $services)
            ->setPrompt('Zvoľte službu.');
        $form->addSelect('customerId', 'Zákazník', $phones)
            ->setPrompt('Zvoľte zákazníka.')
            ->setRequired('Musíte zadať zákazníka.');

        if ($this->customer)
        {
            $customerData = [
                'customerId' => $this->customer->getId(),
            ];
            $form->setDefaults($customerData);
        }

        if ($this->defaultEntity) {

            $i = 1;
            $serviceData = [];
            foreach ($this->defaultEntity->services as $service)
            {
                $serviceData['serviceId' . $i] = $service->id;
                $i++;
            }

            $orderData = [
                'orderId' => $this->defaultEntity->getId(),
                'spz' => $this->defaultEntity->spz,
                'customerId' => $this->defaultEntity->customer->id,
                'sizeId' => $this->defaultEntity->size->id,
                'date' => $this->defaultEntity->startDatetime->format('d.m.Y'),
            ];


            $mergeOrderData = array_merge($orderData, $serviceData);
            $form->setDefaults($mergeOrderData);
        }

        $form->addSubmit('send', 'Uložiť');

        $form->onSuccess[] =[$this, 'bookingFormSucceeded'];

        foreach ($this->onSuccess as $callback) {
            $form->onSuccess[] = $callback;
        }

        return $form;
    }

    public function onValidate($form, $values)
    {
        $customer = $this->customerModel->getCustomerByPhone($values->phone, $values->ico);
        if ($customer) {
            $form['phone']->addError('Zákazník už existuje! Pri vytvorení objednávky použite už existujúceho zákazníka.');
        }
        if($values->serviceId1 == $values->serviceId2 || $values->serviceId1 == $values->serviceId3 || $values->serviceId2 == $values->serviceId2)
        {
            $form->addError('Služba už sa v objednávke nachádza. Vyberte inú');
        }
    }

    /**
     * @param Form $form
     * @param $values
     * @throws \Exception
     */
    public function bookingFormSucceeded(Form $form, $values)
    {
        if ($this->defaultEntity) {

            $newServices = [];
            $temp = 0;

            if($values->serviceId1)
            {
                $s1 = $this->entityManager->getRepository(Service::class)->findOneBy(['id'=>$values->serviceId1]);
                $newServices[] = $s1->id;
            }
            if($values->serviceId2)
            {
                $s2 = $this->entityManager->getRepository(Service::class)->findOneBy(['id'=>$values->serviceId2]);
                $newServices[] = $s2->id;
            }
            if($values->serviceId3)
            {
                $s3 = $this->entityManager->getRepository(Service::class)->findOneBy(['id'=>$values->serviceId3]);
                $newServices[] = $s3->id;
            }

            $oldServices = $this->defaultEntity->services;
            $services = [];
            foreach ($oldServices as $service)
            {
                $services[] = $service->id;
            }

            foreach ($oldServices as $service)
            {
                if (!in_array($service->id, $newServices))
                {
                    $this->defaultEntity->removeService($service);
                }
            }

            foreach ($newServices as $new)
            {
                if (!in_array($new, $services))
                {
                    $newService = $this->serviceModel->getServiceById($new);
                    $this->defaultEntity->addService($newService);
                }
            }

            foreach($this->defaultEntity->services as $service)
            {
                /* if customer chose any option, sizeId is set to no-size, because he choose also service that not support
                        many sizes of car*/
                if($service->use == 'single' && $values->sizeId != 5) {
                    $values->sizeId = 5;
                }

                $price = $this->priceModel->getPrice($service->id, $values->sizeId, $this->defaultEntity->shop->id)->getResult();
                if($price) {
                    $temp += (int)$price[0]['value'];
                    $this->defaultEntity->price = $temp;
                }
            }

            $this->defaultEntity->spz = $values->spz;
            $this->defaultEntity->size = $this->entityManager->getRepository(Size::class)->findOneBy(['id'=>$values->sizeId]);

            $datetime = $values->date . ' ' . $values->time;
            $this->defaultEntity->startDatetime = new DateTime($datetime);

            $this->entityManager->persist($this->defaultEntity);

        } else {
            $booking = new Booking();
            $booking->customer = $this->entityManager->getRepository(Customer::class)->findOneBy(['id'=>$values->customerId]);

            $datetime = $values->date . ' ' . $values->time;

            $booking->startDatetime = new DateTime($datetime);

            $booking->date = new DateTime();

            $services = [
                0 => $this->entityManager->getRepository(Service::class)->findOneBy(['id'=>$values->serviceId1]),
                1 => $this->entityManager->getRepository(Service::class)->findOneBy(['id'=>$values->serviceId2]),
                2 => $this->entityManager->getRepository(Service::class)->findOneBy(['id'=>$values->serviceId3]),
            ];

            $services = array_filter($services);
            $booking->services = $services;

            foreach($services as $service)
            {
                /* if customer chose any option, sizeId is set to no-size, because he choose also service that not support
                        many sizes of car*/
                if($service->use == 'single') {
                    $price = $this->priceModel->getPrice($service->id, 5, $this->employee->shop)->getResult();
                }
                else
                {
                    $price = $this->priceModel->getPrice($service->id, $values->sizeId, $this->employee->shop)->getResult();
                }

                if($price) {
                    $booking->price += (int)$price[0]['value'];
                }
            }
            
            $booking->discount = $this->entityManager->getRepository(Discount::class)->findOneBy(['id'=>1]);
            $booking->size = $this->entityManager->getRepository(Size::class)->findOneBy(['id'=>$values->sizeId]);
            $booking->user = $this->employee;
            $booking->shop = $this->employee->shop;
            $booking->spz = $values->spz;


            $booking->booking_number = $booking->startDatetime->format('YmHis');

            $this->entityManager->persist($booking);
        }
        $this->entityManager->flush();
    }

    /**
     * @param $orderId
     * @throws BadRequestException
     */
    public function editOrder($orderId)
    {
        if (!$this->defaultEntity) {
            $this->flashMessage('Objednávka neexistuje');
            $this->redirect('Homepage:default');
        }
    }
}

interface IBookingForm
{
    /**
     * @return BookingForm
     */
    public function create();
}