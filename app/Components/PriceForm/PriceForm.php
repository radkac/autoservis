<?php

namespace App\Components\PriceForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\Entity\Price;
use App\Model\Entity\Service;
use App\Model\Entity\Shop;
use App\Model\Entity\Size;
use App\Model\PriceModel;
use App\Model\ServiceModel;
use App\Model\ShopModel;
use App\Model\SizeModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class PriceForm extends BaseForm
{
	/**
	 * @var array
	 */
	private $onSuccess = [];
	
	/**
	 * @var PriceModel
	 */
	private $priceModel;
	
	/**
	 * @var ShopModel
	 */
	private $shopModel;
	
	/**
	 * @var ServiceModel
	 */
	private $serviceModel;
	
	/**
	 * @var SizeModel
	 */
	private $sizeModel;
	
	/**
	 * @var EntityManager
	 */
	private $entityManager;
	
	public function __construct(EntityManager $entityManager, PriceModel $priceModel,
			ShopModel $shopModel, ServiceModel $serviceModel, SizeModel $sizeModel)
	{
		$this->entityManager = $entityManager;
		$this->priceModel = $priceModel;
		$this->shopModel = $shopModel;
		$this->serviceModel = $serviceModel;
		$this->sizeModel = $sizeModel;
	}
	
	public function render()
	{
		$this->template->render(__DIR__ . '/templates/priceForm.latte');
	}
	
	public function addSuccessCallback($callback)
	{
		$this->onSuccess[] = $callback;
	}
	
	protected function createComponentPriceForm()
	{
		$shops = $this->shopModel->getShops();
		if(($key = array_search('admin', $shops)) !== false) {
			unset($shops[$key]);
		}
		$services = $this->serviceModel->getServices();
		$sizes = $this->sizeModel->getSizes();
		
		$form = new Form();
		$form->addHidden('id');
		$form->setRenderer(new FormRenderer);
		$form->addSelect('shopId', 'Názov predajne', $shops)
				->setRequired('Musíte zadať názov predajne');
		$form->addSelect('serviceId', 'Služba', $services)
				->setRequired('Musíte zadať službu.');
		$form->addSelect('sizeId', 'Veľkosť', $sizes)
				->setRequired('Musíte zadať veľkosť auta.');
		$form->addText('value', 'Cena (€)')
				->setRequired('Musíte zadať cenu služby.');
		
		if ($this->defaultEntity) {
			$this->defaultEntity = $this->priceModel->getPriceById($this->defaultEntity->id);
			
			$priceData = [
					'id' => $this->defaultEntity->id,
					'shopId' => $this->defaultEntity->shop->id,
					'serviceId' => $this->defaultEntity->service->id,
					'sizeId' => $this->defaultEntity->size->id,
					'value' => $this->defaultEntity->value,
			];
			$form->setDefaults($priceData);
			
		}
		
		
		$form->addSubmit('add', 'Uložiť');
		$form->onSuccess[] = [$this, 'priceFormSucceeded'];
		
		foreach ($this->onSuccess as $callback) {
			$form->onSuccess[] = $callback;
		}
		
		return $form;
	}
	
	public function priceFormSucceeded(Form $form, $values)
	{
		if ($this->defaultEntity) {
			$this->defaultEntity->value = $values->value;
			$this->defaultEntity->size = $this->entityManager->getRepository(Size::class)->findOneBy(['id'=>$values->sizeId]);
			
			$this->entityManager->persist($this->defaultEntity);
		} else {
			$price = new Price();
			$price->shop = $this->entityManager->getRepository(Shop::class)->findOneBy(['id'=>$values->shopId]);
			$price->service = $this->entityManager->getRepository(Service::class)->findOneBy(['id'=>$values->serviceId]);
			$price->size = $this->entityManager->getRepository(Size::class)->findOneBy(['id'=>$values->sizeId]);
			$price->value = $values->value;
	
			$this->entityManager->persist($price);
		}
		$this->entityManager->flush();
	}
}

interface IPriceForm
{
	/**
	 * @return PriceForm
	 */
	public function create();
}