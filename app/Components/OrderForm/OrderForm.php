<?php

namespace App\Components\OrderForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\CarModel;
use App\Model\CustomerModel;
use App\Model\DiscountModel;
use App\Model\Entity\Car;
use App\Model\Entity\Customer;
use App\Model\Entity\Discount;
use App\Model\Entity\History;
use App\Model\Entity\Order;
use App\Model\Entity\OrderService;
use App\Model\Entity\Payment;
use App\Model\Entity\Service;
use App\Model\Entity\Size;
use App\Model\Entity\User;
use App\Model\HistoryModel;
use App\Model\OrderServiceModel;
use App\Model\PaymentModel;
use App\Model\PriceModel;
use App\Model\ServiceModel;
use App\Model\SizeModel;
use Doctrine\Common\Util\Debug;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;
use Tracy\Debugger;

class OrderForm extends BaseForm
{
    /**
     * @var array
     */
    private $onSuccess = [];

    /**
     * @var CarModel
     */
    private $carModel;

    /**
     * @var CustomerModel
     */
    private $customerModel;

    /**
     * @var ServiceModel
     */
    private $serviceModel;

    /**
     * @var PriceModel
     */
    private $priceModel;

    /**
     * @var SizeModel
     */
    private $sizeModel;

    /**
     * @var DiscountModel
     */
    private $discountModel;

    /**
     * @var HistoryModel
     */
    private $historyModel;

    /**
     * @var PaymentModel
     */
    private $paymentModel;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var User
     */
    private $employee;

    /**
     * @var Customer
     */
    private $customer;


    /**
     * OrderForm constructor.
     * @param EntityManager $entityManager
     * @param CarModel $carModel
     * @param CustomerModel $customerModel
     * @param ServiceModel $serviceModel
     * @param SizeModel $sizeModel
     * @param PriceModel $priceModel
     * @param DiscountModel $discountModel
     * @param HistoryModel $historyModel
     * @param PaymentModel $paymentModel
     */
    public function __construct(EntityManager $entityManager, CarModel $carModel, CustomerModel $customerModel,
                                ServiceModel $serviceModel, SizeModel $sizeModel, PriceModel $priceModel,
                                DiscountModel $discountModel, HistoryModel $historyModel, PaymentModel $paymentModel)
    {
        $this->entityManager = $entityManager;
        $this->carModel = $carModel;
        $this->customerModel = $customerModel;
        $this->serviceModel = $serviceModel;
        $this->sizeModel = $sizeModel;
        $this->priceModel = $priceModel;
        $this->discountModel = $discountModel;
        $this->historyModel = $historyModel;
        $this->paymentModel = $paymentModel;
    }

    /**
     * @param $user
     */
    public function setEmployee($user)
    {
        $this->employee = $user;
    }

    /**
     * @param $customerId
     */
    public function setCustomer($customerId)
    {
        $this->customer = $this->customerModel->getCustomerById($customerId);
    }

    public function setServices($orderId)
    {
//        $this->orderServices = $this->orderServiceModel->getServices($orderId)->getResult();
//        Debugger::barDump($this->orderServices, 'orderServices');
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/templates/orderForm.latte');
    }

    /**
     * @param $callback
     */
    public function addSuccessCallback($callback)
    {
        $this->onSuccess[] = $callback;
    }

    /**
     * @return Form
     */
    protected function createComponentOrderForm()
    {
        $cars = $this->carModel->getCars();
        $services = $this->serviceModel->getServices();
        $sizes = $this->sizeModel->getSizes();
        $discounts = $this->discountModel->getDiscounts();
        $payments = $this->paymentModel->getPayments();

        unset($sizes[0]);
        $customersPhone = $this->customerModel->getAllCustomers()->getResult();
        $phones = [];
        foreach($customersPhone as $customer)
        {
            $phones[$customer->id] = $customer->phone;
        }

        $statuses = [
            'inBox' => 'V boxe',
            'washed' => 'Umyté',
            'paid' => 'Zaplatené',
        ];

        $form = new Form();

        $form->setRenderer(new FormRenderer);
        $form->addHidden('id');
        $form->addText('spz', 'ŠPZ')
            ->setRequired('Musíte zadat ŠPZ.')
            ->addRule(Form::PATTERN, ' ŠPZ nesmie obsahovať malé písmená a píše sa bez pomlčky.', '^[A-Z]{2}(([\d]{3})[A-Z]{2}|[A-Z\d]{5})$')
            ->setOption('description', 'ŠPZ nesmie obsahovať malé písmená a píše sa bez pomlčky.');
        $form->addSelect('carId', 'Značka', $cars)
            ->setPrompt('Zvoľte značku auta.')
            ->setRequired('Musíte zadať značku auta.');
        $form->addSelect('sizeId', 'Veľkosť auta', $sizes)
            ->setPrompt('Zvoľte veľkosť auta.')
            ->setRequired('Musíte zadať veľkosť auta.');
        $form->addSelect('serviceId1', 'Služba', $services)
            ->setPrompt('Zvoľte službu.')
            ->setRequired('Musíte zadať službu.');
        $form->addSelect('serviceId2', '+ Služba', $services)
            ->setPrompt('Zvoľte službu.');
        $form->addSelect('serviceId3', '+ Služba', $services)
            ->setPrompt('Zvoľte službu.');
        $form->addSelect('discountId', 'Zľava', $discounts);
        $form->addSelect('paymentId', 'Druh platby', $payments)
            ->setPrompt('Zvoľte druh platby.')
            ->setRequired('Musíte zadať druh platby.');
        $form->addSelect('customerId', 'Zákazník', $phones)
            ->setPrompt('Zvoľte zákazníka.')
            ->setRequired('Musíte zadať zákazníka.');
        $form->addCheckboxList('status', '', $statuses);    // TODO: postupne zaklikavanie, nemoze odkliknut tretie ak niesu prve dve

        if ($this->customer)
        {
            $customerData = [
                'customerId' => $this->customer->getId(),
            ];
            $form->setDefaults($customerData);
        }

        if ($this->defaultEntity) {
            $status = [];

            if($this->defaultEntity->inBox)
            {
                $status[] = 'inBox';
            }
            if($this->defaultEntity->washed)
            {
                $status[] = 'washed';
            }
            if($this->defaultEntity->paid)
            {
                $status[] = 'paid';
            }

            $i = 1;
            $serviceData = [];
            foreach ($this->defaultEntity->services as $service)
            {
                $serviceData['serviceId' . $i] = $service->id;
                $i++;
            }

            $orderData = [
                'orderId' => $this->defaultEntity->getId(),
                'spz' => $this->defaultEntity->spz,
                'carId' => $this->defaultEntity->car->id,
                'customerId' => $this->defaultEntity->customer->id,
                'sizeId' => $this->defaultEntity->size->id,
                'discountId' => $this->defaultEntity->discount->id,
                'paymentId' => $this->defaultEntity->payment->id,
                'status' => $status,
            ];

            $mergeOrderData = array_merge($orderData, $serviceData);
            $form->setDefaults($mergeOrderData);
        }

        $form->addSubmit('send', 'Odoslať');

        $form->onSuccess[] =[$this, 'orderFormSucceeded'];

        foreach ($this->onSuccess as $callback) {
            $form->onSuccess[] = $callback;
        }

        return $form;
    }

    public function onValidate($form, $values)
    {
        $customer = $this->customerModel->getCustomerByPhone($values->phone, $values->ico);
        if ($customer) {
            $form['phone']->addError('Zákazník už existuje! Pri vytvorení objednávky použite už existujúceho zákazníka.');
        }
        if($values->serviceId1 == $values->serviceId2 || $values->serviceId1 == $values->serviceId3 || $values->serviceId2 == $values->serviceId2)
        {
            $form->addError('Služba už sa v objednávke nachádza. Vyberte inú');
        }
    }

    /**
     * @param Form $form
     * @param $values
     * @throws \Exception
     */
    public function orderFormSucceeded(Form $form, $values)
    {
        if ($this->defaultEntity) {

            $newServices = [];
            $temp = 0;

            if($values->serviceId1)
            {
                $s1 = $this->entityManager->getRepository(Service::class)->findOneBy(['id'=>$values->serviceId1]);
                $newServices[] = $s1->id;
            }
            if($values->serviceId2)
            {
                $s2 = $this->entityManager->getRepository(Service::class)->findOneBy(['id'=>$values->serviceId2]);
                $newServices[] = $s2->id;
            }
            if($values->serviceId3)
            {
                $s3 = $this->entityManager->getRepository(Service::class)->findOneBy(['id'=>$values->serviceId3]);
                $newServices[] = $s3->id;
            }

            $oldServices = $this->defaultEntity->services;
            $services = [];
            foreach ($oldServices as $service)
            {
                $services[] = $service->id;
            }

            foreach ($oldServices as $service)
            {
                if (!in_array($service->id, $newServices))
                {
                    $this->defaultEntity->removeService($service);
                }
            }

            foreach ($newServices as $new)
            {
                if (!in_array($new, $services))
                {
                    $newService = $this->serviceModel->getServiceById($new);
                    $this->defaultEntity->addService($newService);
                }
            }

            foreach($this->defaultEntity->services as $service)
            {
                /* if customer chose any option, sizeId is set to no-size, because he choose also service that not support
                        many sizes of car*/
                if($service->use == 'single' && $values->sizeId != 5) {
                    $values->sizeId = 5;
                }

                $price = $this->priceModel->getPrice($service->id, $values->sizeId, $this->defaultEntity->shop->id)->getResult();
                $temp += (int)$price[0]['value'];
                $this->defaultEntity->price = $temp;
            }

            $this->defaultEntity->spz = $values->spz;
            $this->defaultEntity->car = $this->entityManager->getRepository(Car::class)->findOneBy(['id'=>$values->carId]);
            $this->defaultEntity->payment = $this->entityManager->getRepository(Payment::class)->findOneBy(['id'=>$values->paymentId]);

            $this->defaultEntity->inBox = in_array('inBox', $values->status);
            $this->defaultEntity->washed = in_array('washed', $values->status);
            $this->defaultEntity->paid = in_array('paid', $values->status);

            $this->defaultEntity->discount = $this->entityManager->getRepository(Discount::class)->findOneBy(['id'=>$values->discountId]);
            $priceAfterDiscount = $this->defaultEntity->price - (($this->defaultEntity->price/100) * $this->defaultEntity->discount->value);

            $this->defaultEntity->priceAfterDiscount = $priceAfterDiscount;

            $history = new History($values);
            $history->order = $this->defaultEntity;
            $history->datetime = new DateTime();

            if($this->defaultEntity->inBox == 1 && $this->defaultEntity->washed == 1 && $this->defaultEntity->paid == 1)
            {
                $this->defaultEntity->finished = 1;
                $this->defaultEntity->endDatetime = new DateTime();
                $history->description = "Zákazka dokončená";
            }
            else {
                $this->defaultEntity->finished = 0;

                $history->description = "Zmena v zákazke";
            }

            $this->entityManager->persist($history);
            $this->entityManager->persist($this->defaultEntity);

        } else {
            $order = new Order();
            $order->customer = $this->entityManager->getRepository(Customer::class)->findOneBy(['id'=>$values->customerId]);
            $order->car = $this->entityManager->getRepository(Car::class)->findOneBy(['id'=>$values->carId]);

            $services = [
                0 => $this->entityManager->getRepository(Service::class)->findOneBy(['id'=>$values->serviceId1]),
                1 => $this->entityManager->getRepository(Service::class)->findOneBy(['id'=>$values->serviceId2]),
                2 => $this->entityManager->getRepository(Service::class)->findOneBy(['id'=>$values->serviceId3]),
            ];

            $services = array_filter($services);
            $order->services = $services;


            foreach($services as $service)
            {
                /* if customer chose any option, sizeId is set to no-size, because he choose also service that not support
                        many sizes of car*/
                if($service->use == 'single') {
                    $price = $this->priceModel->getPrice($service->id, 5, $this->employee->shop)->getResult();
                }
                else
                {
                    $price = $this->priceModel->getPrice($service->id, $values->sizeId, $this->employee->shop)->getResult();
                }

                if($price) {
                    $order->price += (int)$price[0]['value'];
                }
            }

            $order->size = $this->entityManager->getRepository(Size::class)->findOneBy(['id'=>$values->sizeId]);
            $order->user = $this->employee;
            $order->shop = $this->employee->shop;
            $order->spz = $values->spz;
            $order->inBox = in_array('inBox', $values->status);
            $order->washed = in_array('washed', $values->status);
            $order->paid = in_array('paid', $values->status);
            $order->order_number = $order->startDatetime->format('YmHis');
            $order->payment = $this->entityManager->getRepository(Payment::class)->findOneBy(['id'=>$values->paymentId]);

            $order->discount = $this->entityManager->getRepository(Discount::class)->findOneBy(['id'=>$values->discountId]);
            $priceAfterDiscount = $order->price - (($order->price/100) * $order->discount->value);

            $order->priceAfterDiscount = number_format((float)$priceAfterDiscount, 2, '.', '');

            if($order->inBox == 1 && $order->washed == 1 && $order->paid == 1)
            {
                $order->finished = 1;
                $order->endDatetime = new DateTime();
            }
            else {
                $order->finished = 0;
            }

            $history = new History($values);
            $history->order = $order;
            $history->datetime = new DateTime();
            $history->description = "Vytvorenie zákazky";

            $this->entityManager->persist($order);
            $this->entityManager->persist($history);
        }
        $this->entityManager->flush();
    }

    /**
     * @param $orderId
     * @throws BadRequestException
     */
    public function editOrder($orderId)
    {
        if (!$this->defaultEntity) {
            $this->flashMessage('Zákazka neexistuje');
            $this->redirect('Homepage:default');
        }
    }
}

interface IOrderForm
{
    /**
     * @return OrderForm
     */
    public function create();
}