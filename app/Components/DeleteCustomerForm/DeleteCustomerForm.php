<?php

namespace App\Components\DeleteCustomerForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\CustomerModel;
use App\Model\UserGroupModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;
use Tracy\Debugger;

class DeleteCustomerForm extends BaseForm
{
    /**
     * @var array
     */
    private $onSuccess = [];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var CustomerModel
     */
    private $customerModel;

    /**
     * DeleteForm constructor.
     * @param EntityManager $entityManager
     * @param CustomerModel $customerModel
     */
    public function __construct(EntityManager $entityManager, CustomerModel $customerModel)
    {
        $this->entityManager = $entityManager;
        $this->customerModel = $customerModel;
    }

    /**
     * @param $entity
     */
    public function setDefaultEntity($entity)
    {
        $this->defaultEntity = $entity;
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/templates/deleteCustomerForm.latte');
    }

    /**
     * @param $callback
     */
    public function addSuccessCallback($callback)
    {
        $this->onSuccess[] = $callback;
    }

    /**
     * @return Form
     */
    protected function createComponentDeleteCustomerForm()
    {
        $form = new Form();
        $form->addHidden('id');
        $form->setRenderer(new FormRenderer);
        $form->addSubmit('cancel', 'Späť')
            ->setValidationScope(FALSE)
            ->onClick[] = array($this, 'cancelClicked');
        $form->addSubmit('yes', 'Vymazať');


        $form->onSuccess[] = [$this, 'deleteCustomerFormSucceeded'];

        foreach ($this->onSuccess as $callback) {
            $form->onSuccess[] = $callback;
        }

        Debugger::barDump($this->defaultEntity);

        return $form;
    }

    public function cancelClicked(SubmitButton $button)
    {
        $this->getPresenter()->redirect('Customer:default');
    }

    public function deleteCustomerFormSucceeded(Form $form, $values)
    {
        if($this->defaultEntity) {
            $this->defaultEntity->active = 0;
            $this->entityManager->persist($this->defaultEntity);
        }
        $this->entityManager->flush();
    }
}

interface IDeleteCustomerForm
{
    /**
     * @return DeleteCustomerForm
     */
    public function create();
}