<?php

namespace App\Components;

use Doctrine\Common\Util\Debug;
use Nette\Forms\Controls\Checkbox;
use Nette\Forms\Controls\CheckboxList;
use Nette\Forms\IControl;
use Nette\Forms\Rendering\DefaultFormRenderer;
use Nette\Utils\Html;
use Tracy\Debugger;

/**
 * <code>
 * $form->setRenderer(new App\Components\FormRenderer);
 * </code>
 */
class FormRenderer extends DefaultFormRenderer
{
    /* @var array of HTML tags */
    public $wrappers = [
        'form' => [
            'container' => NULL,
        ],
        'error' => [
            'container' => 'ul',
            'item' => 'li class=alert',
        ],
        'group' => [
            'container' => 'fieldset',
            'label' => 'legend',
            'description' => 'p',
        ],
        'controls' => [
            'container' => 'div',
        ],
        'pair' => [
            'container' => 'div class="form-group row"',
            '.required' => 'required',
            '.optional' => NULL,
            '.odd' => NULL,
            '.error' => 'has-error',
        ],
        'control' => [
            'container' => 'div class=col-sm-10',
            '.odd' => NULL,
            'description' => 'small',
            'requiredsuffix' => '',
            'errorcontainer' => 'span class=help-block',
            'erroritem' => '',
            '.required' => 'required',
            '.text' => 'text',
            '.password' => 'text',
            '.file' => 'text',
            '.submit' => 'btn btn-primary pull-right',
            '.image' => 'imagebutton',
            '.button' => 'button',
        ],
        'label' => [
            'container' => 'div class=col-sm-2',
            'suffix' => NULL,
            'requiredsuffix' => '',
        ],

    ];

    /**
     * Renders 'control' part of visual row of controls.
     * @return string
     */
    public function renderControl(IControl $control)
    {

        $body = $this->getWrapper('control container');

        if ($this->counter % 2) {
            $body->class($this->getValue('control .odd'), TRUE);
        }

        $description = $control->getOption('description');

        if ($description instanceof Html) {
            $description = ' ' . $description;
        } elseif (is_string($description)) {
            $description = ' ' . $this->getWrapper('control description')->setText($control->translate($description));
        } else {
            $description = '';
        }

        if ($control->isRequired()) {
            $description = $this->getValue('control requiredsuffix') . $description;
        }

        $control->setOption('rendered', TRUE);

        if($control instanceof CheckboxList)
        {
            $control->separatorPrototype->setName('');
//            $control->getControlPrototype()->addClass('col-sm-offset-2');

        }

        if ($control instanceof Checkbox OR $control instanceof CheckboxList) {
            $control->getLabelPrototype()->addClass('checkbox-inline');
        }

        $element = $control->getControl();
        if ($element instanceof Html) {
            $element->addClass('form-control');
        }

        if ($element instanceof Html AND $element->getName() === 'input') {
            $element->class($this->getValue("control .$element->type"), TRUE);
        }

        return $body->setHtml($element . $description . $this->renderErrors($control));
    }

    /**
     * Renders single visual row.
     * @return string
     */
//    public function renderPair(\Nette\Forms\IControl $control)
//    {
//        $pair = $this->getWrapper('pair container');
//
//        if(!($control instanceof CheckboxList)) {
//            $pair->add($this->renderLabel($control));
//        }
//
//        $pair->add($this->renderControl($control));
//        $pair->class($this->getValue($control->isRequired() ? 'pair .required' : 'pair .optional'), TRUE);
//        $pair->class($control->hasErrors() ? $this->getValue('pair .error') : NULL, TRUE);
//        $pair->class($control->getOption('class'), TRUE);
//        if (++$this->counter % 2) {
//            $pair->class($this->getValue('pair .odd'), TRUE);
//        }
//        $pair->id = $control->getOption('id');
//        return $pair->render(0);
//    }
}