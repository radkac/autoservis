<?php

namespace App\Components\DeleteBookingForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\BookingModel;
use App\Model\UserGroupModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;
use Tracy\Debugger;

class DeleteBookingForm extends BaseForm
{
    /**
     * @var array
     */
    private $onSuccess = [];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var BookingModel
     */
    private $bookingModel;

    /**
     * DeleteForm constructor.
     * @param EntityManager $entityManager
     * @param BookingModel $bookingModel
     */
    public function __construct(EntityManager $entityManager, BookingModel $bookingModel)
    {
        $this->entityManager = $entityManager;
        $this->bookingModel = $bookingModel;
    }

    /**
     * @param $entity
     */
    public function setDefaultEntity($entity)
    {
        $this->defaultEntity = $entity;
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/templates/deleteBookingForm.latte');
    }

    /**
     * @param $callback
     */
    public function addSuccessCallback($callback)
    {
        $this->onSuccess[] = $callback;
    }

    /**
     * @return Form
     */
    protected function createComponentDeleteBookingForm()
    {
        $form = new Form();
        $form->addHidden('id');
        $form->setRenderer(new FormRenderer);
        $form->addSubmit('cancel', 'Späť')
            ->setValidationScope(FALSE)
            ->onClick[] = array($this, 'cancelClicked');
        $form->addSubmit('yes', 'Vymazať');


        $form->onSuccess[] = [$this, 'deleteBookingFormSucceeded'];

        foreach ($this->onSuccess as $callback) {
            $form->onSuccess[] = $callback;
        }

        Debugger::barDump($this->defaultEntity);

        return $form;
    }

    public function cancelClicked(SubmitButton $button)
    {
        $this->getPresenter()->redirect('Booking:allBookings');
    }

    public function deleteBookingFormSucceeded(Form $form, $values)
    {
        if($this->defaultEntity) {
            $this->entityManager->remove($this->defaultEntity);
        }
        $this->entityManager->flush();
    }
}

interface IDeleteBookingForm
{
    /**
     * @return DeleteBookingForm
     */
    public function create();
}