<?php

namespace App\Components\StornoForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\Entity\Customer;
use App\Model\Entity\History;
use App\Model\Entity\Storno;
use App\Model\Entity\User;
use App\Model\Entity\UserGroup;
use App\Model\StornoModel;
use App\Model\UserGroupModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;
use Nette\Utils\DateTime;
use Tracy\Debugger;

class StornoForm extends BaseForm
{
    /**
     * @var array
     */
    private $onSuccess = [];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var StornoModel
     */
    private $stornoModel;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var User
     */
    private $user;


    /**
     * DeleteForm constructor.
     * @param EntityManager $entityManager
     * @param UserGroupModel $userGroupModel
     */
    public function __construct(EntityManager $entityManager, StornoModel $stornoModel)
    {
        $this->entityManager = $entityManager;
        $this->stornoModel = $stornoModel;
    }

    /**
     * @param $entity
     */
    public function setDefaultEntity($entity)
    {
        $this->defaultEntity = $entity;
    }

    /**
     * @param $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @param $customer
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/templates/stornoForm.latte');
    }

    /**
     * @param $callback
     */
    public function addSuccessCallback($callback)
    {
        $this->onSuccess[] = $callback;
    }

    /**
     * @return Form
     */
    protected function createComponentStornoForm()
    {
        $form = new Form();
        $form->addHidden('id');
        $form->setRenderer(new FormRenderer);
        $form->addTextArea('reason', 'Dôvod storna')
            ->addRule(Form::MIN_LENGTH, 'Dôvod storna musí mať aspoň %d znakov.', 5)
            ->setRequired('Musíte zadať dôvod stornovania.');

        $form->addSubmit('cancel', 'Späť')
            ->setValidationScope(FALSE)
            ->onClick[] = array($this, 'cancelClicked');

        $form->addSubmit('yes', 'Stornovať');
        $form->onSuccess[] = [$this, 'stornoFormSucceeded'];

        foreach ($this->onSuccess as $callback) {
            $form->onSuccess[] = $callback;
        }

        return $form;
    }

    public function cancelClicked(SubmitButton $button)
    {
        $this->getPresenter()->redirect('Order:detailOrder');
    }

    public function stornoFormSucceeded(Form $form, $values)
    {
        if($this->defaultEntity) {
            $storno = new Storno();
            $storno->order = $this->defaultEntity;
            $storno->reason = $values->reason;
            $storno->datetime = new DateTime();
            $storno->user = $this->user;
            $this->defaultEntity->active = false;

            $history = new History();
            $history->order = $this->defaultEntity;
            $history->datetime = new DateTime();
            $history->description = "Storno objednávky";

            $this->entityManager->persist($this->defaultEntity);
            $this->entityManager->persist($storno);
            $this->entityManager->persist($history);
        }
        $this->entityManager->flush();
    }
}

interface IStornoForm
{
    /**
     * @return StornoForm
     */
    public function create();
}