<?php

namespace App\Components\StornoForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\Entity\Customer;
use App\Model\Entity\History;
use App\Model\Entity\Storno;
use App\Model\Entity\User;
use App\Model\Entity\UserGroup;
use App\Model\StornoModel;
use App\Model\UserGroupModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;
use Nette\Utils\DateTime;
use Tracy\Debugger;

class DeleteStornoForm extends BaseForm
{
    /**
     * @var array
     */
    private $onSuccess = [];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var StornoModel
     */
    private $stornoModel;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var User
     */
    private $user;


    /**
     * DeleteForm constructor.
     * @param EntityManager $entityManager
     * @param UserGroupModel $userGroupModel
     */
    public function __construct(EntityManager $entityManager, StornoModel $stornoModel)
    {
        $this->entityManager = $entityManager;
        $this->stornoModel = $stornoModel;
    }

    /**
     * @param $entity
     */
    public function setDefaultEntity($entity)
    {
        $this->defaultEntity = $entity;
    }

    /**
     * @param $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/templates/deleteStornoForm.latte');
    }

    /**
     * @param $callback
     */
    public function addSuccessCallback($callback)
    {
        $this->onSuccess[] = $callback;
    }

    /**
     * @return Form
     */
    protected function createComponentDeleteStornoForm()
    {
        $form = new Form();
        $form->addHidden('id');
        $form->setRenderer(new FormRenderer);
        $form->addSubmit('cancel', 'Späť')
            ->setValidationScope(FALSE)
            ->onClick[] = array($this, 'cancelClicked');
        $form->addSubmit('yes', 'Zrušiť storno');


        $form->onSuccess[] = [$this, 'deleteStornoFormSucceeded'];


        foreach ($this->onSuccess as $callback) {
            $form->onSuccess[] = $callback;
        }

        return $form;
    }

    public function cancelClicked(SubmitButton $button)
    {
        $this->getPresenter()->redirect('Order:detailOrder');
    }

    public function deleteStornoFormSucceeded(Form $form, $values)
    {
        if($this->defaultEntity) {
            $this->order->active = 1;
            $this->entityManager->remove($this->defaultEntity);
            $this->entityManager->persist($this->order);
        }
        $this->entityManager->flush();
    }
}

interface IDeleteStornoForm
{
    /**
     * @return DeleteStornoForm
     */
    public function create();
}