<?php

namespace App\Components\SalesMonthForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\Entity\Shop;
use App\Model\ShopModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class SalesMonthForm extends BaseForm
{
	/**
	 * @var array
	 */
	private $onSuccess = [];

    /**
     * @var Shop
     */
    private $shop;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ShopModel
     */
    private $shopModel;
	
	public function __construct(EntityManager $entityManager, ShopModel $shopModel)
	{
		$this->entityManager = $entityManager;
        $this->shopModel = $shopModel;
    }

    public function setShop($shopId)
    {
        $this->shop = $shopId;
    }
	
	public function render()
	{
		$this->template->render(__DIR__ . '/templates/salesMonthForm.latte');
	}
	
	public function addSuccessCallback($callback)
	{
		$this->onSuccess[] = $callback;
	}
	
	protected function createComponentSalesMonthForm()
	{
		
		$form = new Form();
		$form->addHidden('id');
		$form->addHidden('shopId');
		$form->setRenderer(new FormRenderer);
		$form->addText('month', 'Mesiac')
				->setRequired('Musíte zadať mesiac')
				->setHtmlId('datepickerMonth');


        $salesData = array (
            'shopId' => $this->shop
        );

        $form->setDefaults($salesData);


        $form->addSubmit('show', 'Zobraziť');
		$form->onSuccess[] = [$this, 'salesMonthFormSucceeded'];
		
		foreach ($this->onSuccess as $callback) {
			$form->onSuccess[] = $callback;
		}
		
		return $form;
	}
	
	public function salesMonthFormSucceeded(Form $form, $values)
	{
	}
}

interface ISalesMonthForm
{
	/**
	 * @return SalesMonthForm
	 */
	public function create();
}