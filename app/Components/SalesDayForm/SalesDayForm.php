<?php

namespace App\Components\SalesDayForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\Entity\Shop;
use App\Model\ShopModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class SalesDayForm extends BaseForm
{
	/**
	 * @var array
	 */
	private $onSuccess = [];

    /**
     * @var Shop
     */
	private $shop;
	
	/**
	 * @var EntityManager
	 */
	private $entityManager;

    /**
     * @var ShopModel
     */
    private $shopModel;

    /**
     * SalesDayForm constructor.
     * @param EntityManager $entityManager
     * @param ShopModel $shopModel
     */
	
	public function __construct(EntityManager $entityManager, ShopModel $shopModel)
	{
		$this->entityManager = $entityManager;
		$this->shopModel = $shopModel;
	}
	
	public function render()
	{
		$this->template->render(__DIR__ . '/templates/salesDayForm.latte');
	}

	public function setShop($shopId)
    {
        $this->shop = $this->shopModel->getShopById($shopId);
    }
	
	public function addSuccessCallback($callback)
	{
		$this->onSuccess[] = $callback;
	}
	
	protected function createComponentSalesDayForm()
	{
		
		$form = new Form();
		$form->addHidden('id');
		$form->addHidden('shopId');
		$form->setRenderer(new FormRenderer);
		$form->addText('date', 'Dátum')
				->setRequired('Musíte zadať dátum')
				->setHtmlId('datepicker');


		$salesData = array (
		    'shopId' => $this->shop->getId()
        );

        $form->setDefaults($salesData);
		
		$form->addSubmit('show', 'Zobraziť');
		$form->onSuccess[] = [$this, 'salesDayFormSucceeded'];
		
		foreach ($this->onSuccess as $callback) {
			$form->onSuccess[] = $callback;
		}
		
		return $form;
	}
	
	public function salesDayFormSucceeded(Form $form, $values)
	{

	}
}

interface ISalesDayForm
{
	/**
	 * @return SalesDayForm
	 */
	public function create();
}