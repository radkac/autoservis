<?php

namespace App\Components\CustomerForm;

use App\Components\BaseForm;
use App\Components\FormRenderer;
use App\Model\CustomerModel;
use App\Model\Entity\Customer;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class CustomerForm extends BaseForm
{
    /**
     * @var array
     */
    private $onSuccess = [];

    /**
     * @var CustomerModel
     */
    private $customerModel;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * CustomerForm constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, CustomerModel $customerModel)
    {
        $this->entityManager = $entityManager;
        $this->customerModel = $customerModel;
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/templates/customerForm.latte');
    }

    /**
     * @param $callback
     */
    public function addSuccessCallback($callback)
    {
        $this->onSuccess[] = $callback;
    }

    /**
     * @return Form
     */
    protected function createComponentCustomerForm()
    {
        $form = new Form();
        $form->addHidden('id');
        $form->setRenderer(new FormRenderer);
//        $form->addText('fname', 'Meno')
//            ->setRequired('Musíte zadať meno.')
//            ->addCondition(Form::FILLED, 'Musíte zadať meno.');
//        $form->addText('lname', 'Priezvisko')
//            ->setRequired('Musíte zadať priezvisko.');
        $form->addText('phone', 'Telefón')
            ->setRequired('Musíte zadať telefón.')
            ->addCondition(Form::FILLED, 'Musíte zadať telefón.')
            ->addRule(Form::PATTERN, 'Zadajte telefónne číslo v správnom formáte. napr. 0907123456', '^[+(]{0,2}[0-9 ().-]{9,}');
        $form->addText('ico', 'IČO');
        $form->addCheckbox('icoLess', 'Zákazník nemá IČO');
        $form->addText('address', 'Adresa')
            ->setRequired('Musíte zadať adresu.');

        if ($this->defaultEntity) {
            $this->defaultEntity = $this->customerModel->getCustomerById($this->defaultEntity->getId());

            $customerData = [
                'id' => $this->defaultEntity->getId(),
//                'fname' => $this->defaultEntity->fname,
//                'lname' => $this->defaultEntity->lname,
                'phone' => $this->defaultEntity->phone,
                'ico' => $this->defaultEntity->ico,
                'address' => $this->defaultEntity->address,
            ];

            $form->setDefaults($customerData);
        }

        $form->addSubmit('add', 'Uložiť');

        $form->onValidate[] = array($this, 'onValidate');
        $form->onSuccess[] = [$this, 'customerFormSucceeded'];

        foreach ($this->onSuccess as $callback) {
            $form->onSuccess[] = $callback;
        }

        return $form;
    }

    public function onValidate($form, $values)
    {
        if(!$this->defaultEntity) {
            $customer = $this->customerModel->getCustomerByPhone($values->phone, $values->ico)->getResult();
            if ($customer) {
                $form['phone']->addError('Zákazník už existuje! Pri vytvorení objednávky použite už existujúceho zákazníka.');
            }
        }

        if ($values->icoLess == FALSE && $values->ico == NULL) {
            $form['ico']->addError('Musíte zadať IČO');
            $form['ico']->addRule(Form::PATTERN, 'IČO nemá správny počet číslic.', '^[\d]{6}|[\d]{8}$');
        }
    }

    /**
     * @param Form $form
     * @param $values
     * @throws \Exception
     */
    public function customerFormSucceeded(Form $form, $values)
    {
        if ($this->defaultEntity) {
//            $this->defaultEntity->fname = $values->fname;
//            $this->defaultEntity->lname = $values->lname;
            $this->defaultEntity->phone = $values->phone;
            $this->defaultEntity->ico = $values->ico;
            $this->defaultEntity->address = $values->address;

            $this->entityManager->persist($this->defaultEntity);
        } else {
            $customer = new Customer();
//            $customer->fname = $values['fname'];
//            $customer->lname = $values['lname'];
            $customer->phone = $values['phone'];
            $customer->ico = $values['ico'];
            $customer->address = $values['address'];

            $this->entityManager->persist($customer);
        }
        $this->entityManager->flush();
    }

    /**
     * @param $customerId
     * @throws BadRequestException
     */
    public function editCustomer($customerId)
    {
        if (!$this->defaultEntity) {
            $this->flashMessage('\'Zákazník neexistuje.\'');
            $this->redirect('Homepage:default');
        }
    }
}

interface ICustomerForm
{
    /**
     * @return CustomerForm
     */
    public function create();
}