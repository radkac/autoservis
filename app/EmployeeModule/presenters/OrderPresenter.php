<?php

namespace App\EmployeeModule\Presenters;

use App\Components\OrderForm\IOrderForm;
use App\Components\OrderForm\OrderForm;
use App\Components\StornoForm\IStornoForm;
use App\Model\CustomerModel;
use App\Model\Entity\Customer;
use App\Model\Entity\Order;
use App\Model\Entity\User;
use App\Model\HistoryModel;
use App\Model\OrderModel;
use App\Model\UserModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Application\Responses\JsonResponse;
use Tracy\Debugger;

class OrderPresenter extends BasePresenter {
	/**
	 * @inject
	 * @var EntityManager
	 */
	public $entityManager;

	/**
	 * @inject
	 * @var OrderModel
	 */
	public $orderModel;

	/**
	 * @inject
	 * @var UserModel
	 */
	public $userModel;

	/**
	 * @inject
	 * @var CustomerModel
	 */
	public $customerModel;

	/**
	 * @inject
	 * @var HistoryModel
	 */
	public $historyModel;

	/**
	 * @var Order
	 */
	private $order = null;

	/**
	 * @var User
	 */
	private $employee;

	/**
	 * @var Customer
	 */
	private $customer;

	public function startup() {
		parent::startup();
		$this->employee = $this->userModel->getUserByUsername($this->getUser()->getIdentity()->username);
	}

	/**
	 * @return Form
	 */
	public function createComponentOrderForm() {
		/**
		 * @var OrderForm
		 */
		$form = $this->context->getByType(IOrderForm::class)->create();
		$form->addSuccessCallback($this->onSucceededOrderForm);

		$form->setDefaultEntity($this->order);
		$form->setEmployee($this->employee);

//        $form->setServices($this->order);

		if ($this->customer) {
			$form->setCustomer($this->customer);
		}

		return $form;
	}

	/**
	 * @param Form $form
	 * @param $values
	 */
	public function onSucceededOrderForm(Form $form, $values) {
		$this->flashMessage('Zákazka bola úspešne pridaná.');
		$this->redirect('Homepage:default');
	}


	public function actionAddOrder($customerId = null) {
		if ($customerId) {
			$this->customer = $this->customerModel->getCustomerById($customerId);
		}
		$this->employee = $this->userModel->getUserById($this->user->identity->id);

	}

	public function actionAddCustomerToOrder() {

	}

	/**
	 * @param $id
	 *
	 * @throws BadRequestException
	 */
	public function actionEditOrder($id) {
		$order = $this->orderModel->getOrderById($id);

		if ($order == null) {
            $this->flashMessage('Zákazka neexistuje');
            $this->redirect('Homepage:default');

		}
//        if ($order->isDone()) {
//            $this->flashMessage('Zákazka, ktorú ste vybrali je už dokončená.');
//            $this->redirect('Homepage:default');
//        }
		$this->order = $order;

		$this->template->order = $order;
	}

	public function actionDetailOrder($orderId) {
		$order     = $this->orderModel->getOrderById($orderId);
		$histories = $this->historyModel->getHistoriesByOrderId($orderId)->getResult();


		if ($order == null) {
            $this->flashMessage('Zákazka neexistuje');
            $this->redirect('Homepage:default');

		}
		$this->template->order     = $order;
		$this->template->histories = $histories;
	}

	public function actionSearch($query) {
		$customers = $this->customerModel->getCustomersByLastName($query)->getResult();
		$data      = [];

		foreach ($customers as $customer) {
			$url    = $this->link('Order:addOrder', $customer->id);
			$data[] = [
				'phone' => $customer->phone,
				'ico'  => $customer->ico,
				'url'  => $url
			];
		}
		$this->sendResponse(new JsonResponse($data));

	}


	public function actionStorno($orderId) {
		$order = $this->orderModel->getOrderById($orderId);

		if ( ! $order) {
            $this->flashMessage('Objednávka neexistuje');
            $this->redirect('Homepage:default');
		}

		if ($order) {
			if ($order->active == false || $order->finished == true) {
                $this->flashMessage('Objednávku nie je možné stornovať.');
                $this->redirect('Homepage:default');
			}
		}
		$this->order = $order;
		$actualId    = $this->getUser()->getIdentity()->id;

		$this->template->userId = $actualId;
		$this->template->order  = $order;
	}

	public function createComponentStornoForm() {
		$form = $this->context->getByType(IStornoForm::class)->create();
		$form->addSuccessCallback($this->onSucceededStornoForm);
		$form->setDefaultEntity($this->order);

		$actualId = $this->getUser()->getIdentity()->id;
		$user     = $this->userModel->getUserById($actualId);
		$form->setUser($user);

		return $form;
	}

	public function onSucceededStornoForm(Form $form, $values) {
		$this->flashMessage('Storno zaznamenané.');
		$this->redirect('Homepage:default');
	}
}