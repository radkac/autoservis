<?php

namespace App\EmployeeModule\Presenters;

use App\Model;
use App\Model\CustomerModel;
use App\Model\OrderModel;
use App\Model\UserModel;
use Doctrine\Common\Util\Debug;
use Kdyby\Doctrine\EntityManager;
use Nette;
use Tracy\Debugger;


class HomepagePresenter extends BasePresenter
{
    /**
     * @inject
     * @var EntityManager
     */
    public $entityManager;

    /**
     * @inject
     * @var OrderModel
     */
    public $orderModel;

    /**
     * @inject
     * @var UserModel
     */
    public $userModel;

    /**
     * @inject
     * @var CustomerModel
     */
    public $customerModel;


    public function renderDefault()
    {
    }

    public function actionDefault()
    {
        $user = $this->userModel->getUserById($this->user->identity->id);
        $closeOrders = $this->orderModel->getCloseOrders($this->user->identity->id);
        $uncloseOrders = $this->orderModel->getUncloseOrders($this->user->identity->id);

        $this->template->closeOrders = $closeOrders->getResult();
        $this->template->uncloseOrders = $uncloseOrders->getResult();
        $this->template->user = $user;
    }
}