<?php

namespace App\EmployeeModule\Presenters;

use App\Components\CustomerForm\ICustomerForm;
use App\Model\BookingModel;
use App\Model\CustomerModel;
use App\Model\Entity\Customer;
use App\Model\Entity\User;
use App\Model\OrderModel;
use App\Model\UserModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;

class CustomerPresenter extends BasePresenter
{
    /**
     * @inject
     * @var EntityManager
     */
    public $entityManager;

    /**
     * @inject
     * @var OrderModel
     */
    public $orderModel;

    /**
     * @inject
     * @var UserModel
     */
    public $userModel;

    /**
     * @inject
     * @var CustomerModel
     */
    public $customerModel;

    /**
     * @inject
     * @var BookingModel
     */
    public $bookingModel;

    /**
     * @var User
     */
    private $employee;

    /**
     * @var Customer
     */
    private $customer;

    public function startup()
    {
        parent::startup();
    }

    public function createComponentCustomerForm()
    {
        $form = $this->context->getByType(ICustomerForm::class)->create();
        $form->addSuccessCallback($this->onSucceededCustomerForm);

        $form->setDefaultEntity($this->customer);

        return $form;
    }

    public function onSucceededCustomerForm(Form $form, $values)
    {
        $this->flashMessage('Zákazník vytvorený.');
        $this->redirect('Homepage:default');
    }

    public function actionAddCustomer()
    {
        $this->employee = $this->userModel->getUserById($this->user->identity->id);
    }

    public function actionDetail($customerId)
    {
        $this->customer = $this->customerModel->getCustomerById($customerId);

        if ($this->customer == NULL) {
            $this->flashMessage('Zákazník neexistuje');
            $this->redirect('Homepage:default');
        }
        $closeOrders = $this->customerModel->getCloseOrders($customerId);
        $uncloseOrders = $this->customerModel->getUncloseOrders($customerId);
        $bookings = $this->bookingModel->getBookingsByCustomer($customerId);


        $this->template->customer = $this->customer;
        $this->template->closeOrders = $closeOrders->getResult();
        $this->template->uncloseOrders = $uncloseOrders->getResult();
        $this->template->bookings = $bookings->getResult();
    }
}