<?php

    namespace App\EmployeeModule\Presenters;

    use App\Model\BookingModel;
    use App\Model\CustomerModel;
    use App\Model\UserModel;
    use Nette\Application\Responses\JsonResponse;
    use Nette\Application\Routers\Route;
    use Tracy\Debugger;

    class JsonPresenter extends BasePresenter
    {
        /**
         * @inject
         * @var CustomerModel
         */
        public $customerModel;

        /**
         * @inject
         * @var BookingModel
         */
        public $bookingModel;

        /**
         * @inject
         * @var UserModel
         */
        public $userModel;

        public function actionSearch ($query)
        {
            $customers = $this->customerModel->getCustomersByPhone( $query )->getResult();
            $data = [];

            foreach ($customers as $customer) {
                $url = $this->link( 'Order:addOrder', $customer->id );
                $data[] = [
                    'phone' => $customer->phone,
                    'ico' => $customer->ico,
                    'url' => $url
                ];
            }

            $this->sendResponse( new JsonResponse( $data ) );
            //        $this->template->customers = $customers;

        }

        /**
         *
         */
        public function actionBookings ()
        {
            $user = $this->userModel->getUserById( $this->getUser()->getIdentity()->getId() );
            $shopId = $user->shop->id;
            $bookings = $this->bookingModel->getShopBookings( $shopId );
            $data = [];
            foreach ($bookings->getResult() as $booking) {
                $data[] = [
                    'titles' => $booking->customer->phone,
                    'start' => $booking->startDatetime->format( 'Y-m-d H:i:s' ),
                    'editable' => "true",
                    'url' => '/employee/booking/edit-booking?bookingId=' . $booking->id
                ];
            }

            $this->sendResponse( new JsonResponse( $data ) );
            //        $this->sendResponse(new JsonResponse(['test' => 'ahoj']));
            $this->template->bookings = $data;
        }

    }