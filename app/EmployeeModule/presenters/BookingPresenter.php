<?php

namespace App\EmployeeModule\Presenters;

use App\Components\BookingForm\BookingForm;
use App\Components\BookingForm\IBookingForm;
use App\Components\DeleteBookingForm\DeleteBookingForm;
use App\Components\DeleteBookingForm\IDeleteBookingForm;
use App\Model\BookingModel;
use App\Model\CustomerModel;
use App\Model\Entity\Booking;
use App\Model\Entity\User;
use App\Model\UserModel;
use Doctrine\Common\Util\Debug;
use Kdyby\Doctrine\EntityManager;
use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;


class BookingPresenter extends BasePresenter
{
    /**
     * @inject
     * @var EntityManager
     */
    public $entityManager;

    /**
     * @inject
     * @var BookingModel
     */
    public $bookingModel;

    /**
     * @inject
     * @var UserModel
     */
    public $userModel;

    /**
     * @inject
     * @var CustomerModel
     */
    public $customerModel;

    /**
     * @var User
     */
    public $employee;

    /**
     * @var Booking
     */
    public $booking;


    public function renderDefault()
    {
    }

    public function createComponentBookingForm()
    {
        /**
         * @var BookingForm
         */
        $form = $this->context->getByType(IBookingForm::class)->create();
        $form->addSuccessCallback($this->onSucceeded);
        $form->setDefaultEntity($this->booking);

        $form->setEmployee($this->employee);

        return $form;
    }

    public function onSucceeded(Form $form, $values)
    {
        $this->flashMessage('Objednávka uložená.');
        $this->redirect('Booking:allBookings');
    }

    public function actionAllBookings()
    {
        $user = $this->userModel->getUserById($this->user->identity->id);
        $bookings = $this->bookingModel->getUserBookings($user->id);

        $this->template->bookings = $bookings->getResult();
        $this->template->user = $this->employee = $user;
    }

    public function createComponentDeleteBookingForm()
    {
        /**
         * @var DeleteBookingForm
         */
        $form = $this->context->getByType(IDeleteBookingForm::class)->create();
        $form->addSuccessCallback($this->onSucceededDeleteBookingForm);
        $form->setDefaultEntity($this->booking);

        return $form;
    }

    public function onSucceededDeleteBookingForm(Form $form, $values)
    {
        $this->flashMessage('Objednávka bola vymazaná.');
        $this->redirect('Booking:allBookings');
    }

    public function actionAddBooking()
    {
        $user = $this->userModel->getUserById($this->user->identity->getId());
        $this->template->user = $this->employee = $user;
    }

    public function actionEditBooking($bookingId)
    {
        $booking = $this->bookingModel->getBookingById($bookingId);

        $this->template->booking = $this->booking = $booking;
    }

    public function actionDetailBooking($bookingId)
    {
        $booking = $this->bookingModel->getBookingById($bookingId);

        $this->template->booking = $this->booking = $booking;
    }


    public function actionDeleteBooking($bookingId)
    {
        $booking = $this->bookingModel->getBookingById($bookingId);

        $this->template->booking = $this->booking = $booking;
    }


    public function actionBookings()
    {
        $user = $this->userModel->getUserById($this->getUser()->getIdentity()->getId());

        $shopId = $user->shop->id;
        $this->template->bookings = $this->bookingModel->getShopBookings($shopId)->getResult();
    }

}