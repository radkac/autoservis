<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
//		Route::$defaultFlags = Route::SECURED;
	
		$routerAdmin = new RouteList('Admin');
		$routerAdmin[] = new Route('admin/<presenter>/<action>[/<id>]', 'Homepage:default');

		$routerEmployee = new RouteList('Employee');
		$routerEmployee[] = new Route('employee/<presenter>/<action>[/<id>]', 'Homepage:default');

		$routerFront = new RouteList('Front');
		$routerFront[] = new Route('<presenter>/<action>[/<id>]', 'Sign:in');

		$router = new RouteList();
		$router[] = new Route('index.php', 'Front:Homepage:default', Route::ONE_WAY);

		$router[] = $routerAdmin;
		$router[] = $routerEmployee;
		$router[] = $routerFront;
		
		return $router;
	}
}
