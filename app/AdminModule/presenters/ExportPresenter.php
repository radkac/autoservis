<?php

namespace App\AdminModule\Presenters;


use App\Components\DeleteForm\IDeleteForm;
use App\Components\PasswordForm\IPasswordForm;
use App\Components\UserForm\IUserForm;
use App\Components\UserForm\UserForm;
use App\Model\BookingModel;
use App\Model\Entity\User;
use App\Model\Entity\UserGroup;
use App\Model\OrderModel;
use App\Model\ShopModel;
use App\Model\UserGroupModel;
use App\Model\UserModel;
use Doctrine\Common\Util\Debug;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class ExportPresenter extends BasePresenter
{
    /**
     * @inject
     * @var UserModel
     */
    public $userModel;

    /**
     * @inject
     * @var OrderModel
     */
    public $orderModel;

    /**
     * @inject
     * @var UserGroupModel
     */
    public $userGroupModel;

    /**
     * @inject
     * @var ShopModel
     */
    public $shopModel;

    /**
     * @inject
     * @var BookingModel
     */
    public $bookingModel;


    public function actionExportUser($userId, $userName)
    {
        $name = explode(" ", $userName);
        $name = $name[0] . $name[1];

        $closeOrders = $this->orderModel->getCloseOrders($userId);
        $uncloseOrders = $this->orderModel->getUncloseOrders($userId);

        $this->template->closeOrders = $closeOrders->getResult();
        $this->template->uncloseOrders = $uncloseOrders->getResult();
        $this->template->name = $name;

    }

    public function actionExportUsers($shopId)
    {
        $shop = $this->shopModel->getShopById($shopId);

        $this->template->shop = $shop;
    }

    public function actionExportDetailUsers($shopId)
    {
        $shop = $this->shopModel->getShopById($shopId);

        $this->template->shop = $shop;
    }
    
    public function actionExportOrdersByDay($userId, $userName)
    {
        $name = explode(" ", $userName);
        $name = $name[0] . $name[1];

        $orders = $this->orderModel->getOrdersByUser($userId)->getResult();

        $ord = [];
        foreach($orders as $order)
        {
            $ordersByDate = $this->orderModel->getOrderByEndDate($order->endDatetime)->getResult();
            $ord[$order->endDatetime->format('Y-m-d')] = $ordersByDate;
        }

        $reverseOrders = array_reverse($ord, true);

        $this->template->ordersByDate = $reverseOrders;
        $this->template->name = $name;
    }

    public function actionExportBookingsByDay($shopId)
    {
        $shop = $this->shopModel->getShopById($shopId);
        $bookings = $this->bookingModel->getBookingsByShop($shopId)->getResult();

        $book = [];
        foreach($bookings as $booking)
        {
            $bookingsByDate = $this->bookingModel->getBookingByDate($booking->date)->getResult();
            $book[$booking->date->format('Y-m-d')] = $bookingsByDate;

        }

        $reverseBookings = array_reverse($book, true);

        $this->template->bookingsByDate = $reverseBookings;
        $this->template->shop = $shop;
    }

    public function actionExportBookingsByShop($shopId = null)
    {
        if($shopId == null) {
            $shopId = 1;
        }
        $shop = $this->shopModel->getShopById($shopId);
        $booking = $this->bookingModel->getBookingsByShop($shopId)->getResult();

        $this->template->bookings = $booking;
        $this->template->shop = $shop;
    }

    public function actionExportAllBookings()
    {
        $booking = $this->bookingModel->getBookings()->getResult();

        $this->template->bookings = $booking;
    }

    public function actionExportSalesByDay($shopId, $date, $month)
    {
        Debugger::barDump($shopId);
        $shop = $this->shopModel->getShopById($shopId);
        $orders = $this->orderModel->getSalesCloseOrders($shopId)->getResult();

        $sales = $this->orderModel->getSalesByDate($shopId, $date, $month, $orders);

        $this->template->todayOrders = $sales['todayOrders'];
        $this->template->monthOrders = $sales['monthOrders'];
        $this->template->month = $month;
        $this->template->date = $date;
        $this->template->shop = $shop;
    }

    public function actionExportSalesByMonth($shop, $date, $month)
    {

    }

}