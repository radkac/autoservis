<?php

namespace App\AdminModule\Presenters;

use App\Model\Entity\UserGroup;
use Nette;
use App\Model;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Presenter;
use Tracy\Debugger;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Presenter
{
    public function startup() {
        parent::startup();

        if(!$this->user->isLoggedIn())
        {
            $this->flashMessage('Pred vstupom do administrácie je nutné sa najskôr prihlásiť.');
            $this->redirect(':Front:Sign:in');
        }

        if($this->user->identity->groupType != UserGroup::GROUP_ADMIN)
        {
            throw new BadRequestException('Nepovolená požiadavka.');
        }
    }

    public function beforeRender()
    {
        $filePath = $this->context->parameters['filePath'];
        $debug = !$this->context->parameters['productionMode'];
        $username = $this->getUser()->getIdentity()->username;
        
        $this->template->debugMode = $debug;
        $this->template->username = $username;
        $this->template->filePath = $filePath;
    }
}
