<?php

namespace App\AdminModule\Presenters;


use App\Components\DeleteForm\DeleteForm;
use App\Components\DeleteForm\IDeleteForm;
use App\Components\PasswordForm\IPasswordForm;
use App\Components\UserForm\IUserForm;
use App\Components\UserForm\UserForm;
use App\Model\BookingModel;
use App\Model\Entity\User;
use App\Model\Entity\UserGroup;
use App\Model\OrderModel;
use App\Model\UserGroupModel;
use App\Model\UserModel;
use Doctrine\Common\Util\Debug;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class UsersPresenter extends BasePresenter
{
    /**
     * @inject
     * @var UserModel
     */
    public $userModel;

    /**
     * @inject
     * @var OrderModel
     */
    public $orderModel;

    /**
     * @inject
     * @var UserGroupModel
     */
    public $userGroupModel;

    /**
     * @inject
     * @var BookingModel
     */
    public $bookingModel;

    /**
     * @var User
     */
    private $user = null;

    public function getUserById($userId)
    {
        $user = $this->userModel->getUserById($userId);
        $this->user = $user;
	
	      $this->isUser($userId);

		    
        $this->template->user = $this->user;
    }
	
		public function isUser($userId) {
			if(! $this->user) {
                $this->flashMessage('Zamestnanec neexistuje');
                $this->redirect('Homepage:default');
			}
			if(! ($this->user->group->type == UserGroup::GROUP_USER)) {
                $this->flashMessage('Informácie o zamestnancovi niesú dostupné.');
                $this->redirect('Homepage:default');
			}
		}


    public function actionDefault()
    {
        $users = $this->userModel->getUsers()->getResult();
        $groups = $this->userGroupModel->getGroupUsers();
        $actualId = $this->getUser()->getIdentity()->id;

        $this->template->users = $users;
        $this->template->groups = $groups;
        $this->template->userId = $actualId;
    }

    public function createComponentUserForm()
    {
        /**
         * @var UserForm
         */
        $form = $this->context->getByType(IUserForm::class)->create();
        $form->addSuccessCallback($this->onSucceededUserForm);
        $form->setDefaultEntity($this->user);

        return $form;
    }

    public function onSucceededUserForm(Form $form, $values)
    {
        $this->flashMessage('Zamestnanec uložený.');
        $this->redirect('Users:default');
    }

    public function createComponentPasswordForm()
    {
        $form = $this->context->getByType(IPasswordForm::class)->create();
        $form->addSuccessCallback($this->onSucceededPasswordForm);

        $form->setDefaultEntity($this->user);
        $form->enableAdmin();

        return $form;
    }

    public function onSucceededPasswordForm(Form $form, $values)
    {
        $this->flashMessage('Heslo uložené.');
        $this->redirect('this');
    }

    public function createComponentDeleteForm()
    {
        /**
         * @var DeleteForm
         */
        $form = $this->context->getByType(IDeleteForm::class)->create();
        $form->addSuccessCallback($this->onSucceededDeleteForm);
        $form->setDefaultEntity($this->user);

        return $form;
    }

    public function onSucceededDeleteForm(Form $form, $values)
    {
        $this->flashMessage('Zamestnanca odteraz nájdete v sekcii Bývalí zamestnanci.');
        $this->redirect('Users:default');
    }


    public function actionEditUser($userId)
    {
        $this->getUserById($userId);
    }

    public function actionChangePassword($userId)
    {
        $this->getUserById($userId);
    }

    public function actionDeleteUser($userId)
    {
        $this->getUserById($userId);
    }

    public function actionUserOrders($userId)
    {
        $this->getUserById($userId);
        
        $closeOrders = $this->orderModel->getCloseOrders($this->user->getId());
        $uncloseOrders = $this->orderModel->getUncloseOrders($this->user->getId());

        $this->template->closeOrders = $closeOrders->getResult();
        $this->template->uncloseOrders = $uncloseOrders->getResult();
    }
    
    public function actionOrdersByDay($userId)
    {
        $this->getUserById($userId);
        
        $orders = $this->orderModel->getOrdersByUser($userId)->getResult();
        $ord = [];
        foreach($orders as $order)
        {
            $ordersByDate = $this->orderModel->getOrderByEndDate($order->endDatetime)->getResult();
            if ($order->finished == 1) {
                $ord[$order->endDatetime->format('Y-m-d')] = $ordersByDate;
            }
        }

        $reverseOrders = array_reverse($ord, true);
        $this->template->ordersByDate = $reverseOrders;
    }

    public function actionDetailUser($userId)
    {
        $this->getUserById($userId);

        $closeOrders = $this->orderModel->getCloseOrders($this->user->getId());
        $uncloseOrders = $this->orderModel->getUncloseOrders($this->user->getId());

        $this->template->closeOrders = $closeOrders->getResult();
        $this->template->uncloseOrders = $uncloseOrders->getResult();
    }

    public function actionGroupUsers($groupId)
    {
        $group = $this->userGroupModel->getGroupById($groupId);
        $actualId = $this->getUser()->getIdentity()->getId();
        $groups = $this->userGroupModel->getGroupUsers();

        $this->template->groups = $groups;

        $this->template->group = $group;
        $this->template->userId = $actualId;

    }

    public function actionUsersHead()
    {
 
    }
    
    public function actionCloseOrders($userId)
    {
	    $this->getUserById($userId);
	    $closeOrders = $this->orderModel->getCloseOrders($this->user->getId());
	
	    $this->template->closeOrders = $closeOrders->getResult();
	
    }
    
    public function actionUncloseOrders($userId)
    {
	    $this->getUserById($userId);
	    $uncloseOrders = $this->orderModel->getUncloseOrders($this->user->getId());
	
	    $this->template->uncloseOrders = $uncloseOrders->getResult();
    	
    }
    
    

}