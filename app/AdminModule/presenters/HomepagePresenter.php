<?php

namespace App\AdminModule\Presenters;

use App\Components\UserForm\IUserForm;
use App\Components\UserForm\UserForm;
use App\Model\Entity\Order;
use App\Model\Entity\Shop;
use App\Model\Entity\User;
use App\Model\UserModel;
use Kdyby\Doctrine\EntityManager;
use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;
use Tracy\Debugger;


class HomepagePresenter extends BasePresenter
{
    /**
     * @inject
     * @var EntityManager
     */
    public $entityManager;

    /**
     * @inject
     * @var UserModel
     */
    public $userModel;

    /**
     * @var User
     */
    private $user = null;

    public function renderDefault()
    {

    }

    public function actionDefault()
    {
//        $orders = $this->orderModel->getOrders();
//        $this->template->orders = $orders->getResult();
        $this->redirect("Users:default");
    }

    public function createComponentUserForm()
    {
        /**
         * @var UserForm
         */
        $form = $this->context->getByType(IUserForm::class)->create();
        $form->addSuccessCallback($this->onSucceeded);
        $form->setDefaultEntity($this->user);

        return $form;
    }

    public function onSucceeded(Form $form, $values)
    {
        $this->flashMessage('Zamestnanec vytvorený.');
        $this->redirect('Homepage:users');
    }

    public function actionAdd()
    {

    }

    public function actionUsers()
    {
        $users = $this->userModel->getUsers();

        $this->template->users = $users->getResult();
    }

    public function actionEdit($id)
    {
//        $order = $this->orderModel->getOrderById($id);
//        Debugger::barDump($order);

        //TODO: check if is in database

//        $this->order = $order;
    }

}