<?php

namespace App\AdminModule\Presenters;

use App\Components\CustomerForm\ICustomerForm;
use App\Components\DeleteCustomerForm\DeleteCustomerForm;
use App\Components\DeleteCustomerForm\IDeleteCustomerForm;
use App\Model\BookingModel;
use App\Model\CustomerModel;
use App\Model\Entity\Customer;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class CustomerPresenter extends BasePresenter
{
    /**
     * @inject
     * @var EntityManager
     */
    public $entityManager;

    /**
     * @inject
     * @var CustomerModel
     */
    public $customerModel;

    /**
     * @inject
     * @var BookingModel
     */
    public $bookingModel;

    /**
     * @var Customer
     */
    private $customer;


    public function actionDefault()
    {
        $customers = $this->customerModel->getAllCustomers()->getResult();
        $this->template->customers = $customers;
    }

    public function actionDelete($customerId)
    {
        $customer = $this->customerModel->getCustomerById($customerId);
        $uncloseOrders = $this->customerModel->getUncloseOrders($customerId)->getResult();
        $user = $this->user->identity->getId();

        $this->customer = $customer;

        $this->template->customer = $customer;
        $this->template->user = $user;
        $this->template->uncloseOrders = $uncloseOrders;
    }


    public function createComponentDeleteCustomerForm()
    {
        /**
         * @var DeleteCustomerForm
         */
        $form = $this->context->getByType(IDeleteCustomerForm::class)->create();
        $form->addSuccessCallback($this->onSucceededDeleteCustomerForm);
        $form->setDefaultEntity($this->customer);

        return $form;
    }

    public function onSucceededDeleteCustomerForm(Form $form, $values)
    {
        $this->flashMessage('Zákazník bol vymazaný.');
        $this->redirect('Customer:default');
    }

    public function actionDetail($customerId)
    {
        $customer = $this->customerModel->getCustomerById($customerId);

        if ($customer == NULL) {
            $this->flashMessage('Zákazník neexistuje');
            $this->redirect('Homepage:default');

        }
        $closeOrders = $this->customerModel->getCloseOrders($customerId);
        $uncloseOrders = $this->customerModel->getUncloseOrders($customerId);
        $bookings = $this->bookingModel->getBookingsByCustomer($customerId);

        $this->template->customer = $customer;
        $this->template->closeOrders = $closeOrders->getResult();
        $this->template->uncloseOrders = $uncloseOrders->getResult();
        $this->template->bookings = $bookings->getResult();
    }

    public function createComponentCustomerForm()
    {
        $form = $this->context->getByType(ICustomerForm::class)->create();
        $form->addSuccessCallback($this->onSucceededCustomerForm);

        $form->setDefaultEntity($this->customer);

        return $form;
    }

    public function onSucceededCustomerForm(Form $form, $values)
    {
        $this->flashMessage('Zákazník upravený.');
        $this->redirect('Customer:default');
    }

    public function actionEdit($customerId)
    {
        $customer = $this->customerModel->getCustomerById($customerId);
        $this->customer = $customer;

    }
}