<?php

namespace App\AdminModule\Presenters;


use App\Components\ShopForm\IShopForm;
use App\Model\Entity\Shop;
use App\Model\OrderModel;
use App\Model\ShopModel;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class ShopsPresenter extends BasePresenter
{
    /**
     * @inject
     * @var ShopModel
     */
    public $shopModel;

    /**
     * @inject
     * @var OrderModel
     */
    public $orderModel;

    /**
     * @var Shop
     */
    private $shop = null;
    
    public function actionDefault()
    {
        $shops = $this->shopModel->getShopsById();
        $this->template->shops = $shops->getResult();
    }

    public function createComponentShopForm()
    {
        $form = $this->context->getByType(IShopForm::class)->create();
        $form->addSuccessCallback($this->onSucceededShopForm);
        $form->setDefaultEntity($this->shop);

        return $form;
    }

    public function onSucceededShopForm(Form $form, $values)
    {
        $this->flashMessage('Prevádzka uložená.');
        $this->redirect('Shops:default');
    }

    public function actionAdd($shopId)
    {

    }

    public function actionEdit($shopId)
    {
        $shop = $this->shopModel->getShopById($shopId);

        if(!$shop)
        {
            $this->flashMessage('Prevádzka neexistuje');
            $this->redirect('Homepage:default');
        }
        $this->shop = $shop;
        $this->template->shop = $shop;
    }

    public function actionDetail($shopId)
    {
        $shop = $this->shopModel->getShopById($shopId);
        if(!$shop)
        {
            $this->flashMessage('Prevádzka neexistuje');
            $this->redirect('Homepage:default');
        }
        $this->shop = $shop;
        $actualId = $this->getUser()->getIdentity()->id;

        $this->template->userId = $actualId;
        $this->template->shop = $shop;
    }

    public function actionOrders($shopId)
    {
        $shop = $this->shopModel->getShopById($shopId);

        $closeOrders = $this->orderModel->getShopCloseOrders($shopId)->getResult();
        $uncloseOrders = $this->orderModel->getShopUncloseOrders($shopId)->getResult();

        $this->template->shop = $shop;
        $this->template->closeOrders = $closeOrders;
        $this->template->uncloseOrders = $uncloseOrders;
    }
}