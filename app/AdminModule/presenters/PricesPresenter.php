<?php

namespace App\AdminModule\Presenters;

use App\Components\PriceForm\IPriceForm;
use App\Components\UserForm\IUserForm;
use App\Components\UserForm\UserForm;
use App\Model\Entity\Order;
use App\Model\Entity\Price;
use App\Model\Entity\Shop;
use App\Model\Entity\User;
use App\Model\UserModel;
use Kdyby\Doctrine\EntityManager;
use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;
use Tracy\Debugger;


class PricesPresenter extends BasePresenter
{
	/**
	 * @inject
	 * @var EntityManager
	 */
	public $entityManager;
	
	/**
	 * @inject
	 * @var Model\PriceModel
	 */
	public $priceModel;
	
	/**
	 * @var Price
	 */
	public $price = null;
	
	
	public function actionDefault()
	{
		
		$prices = $this->priceModel->getPrices()->getResult();
		$shops = [];
		
		foreach($prices as $price){
			$shops[$price->shop->id][$price->id] = $price;
		}
		
		$this->template->shops = $shops;
		
	}
	
	public function actionEdit($priceId)
	{
		$price = $this->priceModel->getPriceById($priceId);
		
		$this->template->price = $this->price = $price;
		
	}
	
	public function createComponentPriceForm()
	{
		$form = $this->context->getByType(IPriceForm::class)->create();
		$form->addSuccessCallback($this->onSucceededPriceForm);
		$form->setDefaultEntity($this->price);
		
		return $form;
	}
	
	public function onSucceededPriceForm(Form $form, $values)
	{
		$this->flashMessage('Cena bola uložená.');
		$this->redirect('Prices:default');
	}
	
}