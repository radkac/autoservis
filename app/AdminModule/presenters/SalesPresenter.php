<?php

namespace App\AdminModule\Presenters;


use App\Components\SalesDayForm\ISalesDayForm;
use App\Components\SalesDayForm\SalesDayForm;
use App\Components\SalesMonthForm\ISalesMonthForm;
use App\Components\SalesMonthForm\SalesMonthForm;
use App\Model\OrderModel;
use App\Model\ShopModel;
use Doctrine\Common\Util\Debug;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;
use Tracy\Debugger;

class SalesPresenter extends BasePresenter {
	/**
	 * @inject
	 * @var OrderModel
	 */
	public $orderModel;

    /**
     * @inject
     * @var ShopModel
     */
    public $shopModel;

    /** @persistent */
    public $dateI;

    /** @persistent */
    public $monthI;

    /** @persistent */
    public $shopId;
	

	public function actionDefault($shopId = null, $date = null, $month = null)
	{
        if($shopId == null)
        {
            $shopId = $this->shopId = 1;
        }

        if($date == null && $month == null)
        {
            $date = new DateTime();
            $date = $date->format('d.m.Y');
        }

        $shop = $this->shopModel->getShopById($shopId);
        $orders = $this->orderModel->getSalesCloseOrders($shopId)->getResult();

        $sales = $this->orderModel->getSalesByDate($shopId, $date, $month, $orders);
        // One day sales

		$this->template->todayOrders = $sales['todayOrders'];
		$this->template->monthOrders = $sales['monthOrders'];
		$this->template->daySalesInfo = $sales['daySalesInfo'];
		$this->template->monthSalesInfo = $sales['monthSalesInfo'];
		$this->template->date = $sales['date'];
		$this->template->month = $sales['month'];
		$this->template->thisShop = $thisShop = $shop;
        $this->template->shops = $this->shopModel->getShopsById()->getResult();

        Debugger::barDump( $sales['month']);
    }
	

	
	public function createComponentSalesDayForm()
	{
		/**
		 * @var SalesDayForm
		 */
		$form = $this->context->getByType(ISalesDayForm::class)->create();
		$form->addSuccessCallback($this->onSucceededSalesDayForm);
		$form->setShop($this->shopId);
		
		return $form;
	}
	
	public function onSucceededSalesDayForm(Form $form, $values)
	{
		$this->redirect('Sales:default', array("date" => $values->date, "shopId" => $values->shopId));
	}
	
	public function createComponentSalesMonthForm()
	{
		/**
		 * @var SalesMonthForm
		 */
		$form = $this->context->getByType(ISalesMonthForm::class)->create();
		$form->addSuccessCallback($this->onSucceededSalesMonthForm);
        $form->setShop($this->shopId);


        return $form;
	}
	
	public function onSucceededSalesMonthForm(Form $form, $values)
	{
		$this->redirect('Sales:default', array("month" => $values->month, "shopId" => $values->shopId));
	}
	
}