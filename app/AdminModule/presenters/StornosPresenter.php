<?php

namespace App\AdminModule\Presenters;


use App\Components\StornoForm\IDeleteStornoForm;
use App\Components\StornoForm\IStornoForm;
use App\Model\CustomerModel;
use App\Model\Entity\Customer;
use App\Model\Entity\Order;
use App\Model\Entity\Storno;
use App\Model\Entity\User;
use App\Model\HistoryModel;
use App\Model\OrderModel;
use App\Model\StornoModel;
use App\Model\UserModel;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class StornosPresenter extends BasePresenter
{
    /**
     * @inject
     * @var StornoModel
     */
    public $stornoModel;
    
    /**
     * @inject
     * @var HistoryModel
     */
    public $historyModel;

    /**
     * @inject
     * @var OrderModel
     */
    public $orderModel;

    /**
     * @inject
     * @var CustomerModel
     */
    public $customerModel;

    /**
     * @inject
     * @var UserModel
     */
    public $userModel;

    /**
     * @var Storno
     */
    private $storno = null;

    /**
     * @var Order
     */
    private $order = null;

    /**
     * @var Customer
     */
    private $customer = null;

    /**
     * @var User
     */
    private $user = null;

    public function actionDefault()
    {
        $stornos = $this->stornoModel->getAllStornos()->getResult();
        $stor = [];
        foreach($stornos as $storno)
        {
            $stornosByDate = $this->stornoModel->getStornosByDate($storno->datetime)->getResult();
            $stor[$storno->datetime->format('Y-m-d')] = $stornosByDate;
        }

        $reverseStornos = array_reverse($stor, true);

        foreach($reverseStornos as $storno)
        {
            foreach($storno as $s)
            {
                Debugger::barDump($s->order->startDatetime->format('d.m.Y'));
            }
        }

        $this->template->stornosByDate = $stor;

        $this->template->stornos = $stornos;
    }

    public function actionDetail($stornoId)
    {
        $storno = $this->stornoModel->getStornoById($stornoId);
        $histories = $this->historyModel->getHistoriesByOrderId($storno->order->id)->getResult();

        if(!$storno)
        {
            $this->flashMessage('Storno neexistuje');
            $this->redirect('Homepage:default');
        }

        $this->template->storno = $storno;
        $this->template->histories = $histories;
    }

    public function actionStorno($orderId, $customerId, $userId)
    {
        $order = $this->orderModel->getOrderById($orderId);
        $customer = $this->customerModel->getCustomerById($customerId);
        $user = $this->userModel->getUserById($userId);

        if(!$order)
        {
            $this->flashMessage('Objednávka neexistuje');
            $this->redirect('Homepage:default');
        }
        if($order)
        {
            if($order->active == FALSE || $order->finished == TRUE )
            {
                $this->flashMessage('Objednávku nie je možné stornovať.');
                $this->redirect('Homepage:default');
            }
        }

        $this->order = $order;
        $this->customer = $customer;
        $this->user = $user;

        $this->template->order = $order;
        $this->template->customer = $customer;
        $this->template->user = $user;
    }

    public function createComponentStornoForm()
    {
        $form = $this->context->getByType(IStornoForm::class)->create();
        $form->addSuccessCallback($this->onSucceededStornoForm);
        $form->setCustomer($this->customer);
        $form->setUser($this->user);
        $form->setDefaultEntity($this->order);

        return $form;
    }

    public function onSucceededStornoForm(Form $form, $values)
    {
        $this->flashMessage('Storno zaznamenané.');
        $this->redirect('Customer:delete', $values->customerId);
    }

    public function actionReturn($stornoId)
    {
        $storno = $this->stornoModel->getStornoById($stornoId);
        $order = $this->orderModel->getOrderById($storno->order->id);
        $histories = $this->historyModel->getHistoriesByOrderId($storno->order->id)->getResult();

        $this->storno = $storno;
        $this->order = $order;

        $this->template->order = $order;
        $this->template->storno = $storno;
        $this->template->histories = $histories;

    }

    public function createComponentDeleteStornoForm()
    {
        $form = $this->context->getByType(IDeleteStornoForm::class)->create();
        $form->addSuccessCallback($this->onSucceededDeleteStornoForm);
        $form->setOrder($this->order);
        $form->setDefaultEntity($this->storno);

        return $form;
    }

    public function onSucceededDeleteStornoForm(Form $form, $values)
    {
        $this->flashMessage('Storno vymazané.');
        $this->redirect('Stornos:default');
    }

}