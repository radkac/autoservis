<?php

namespace App\AdminModule\Presenters;

use App\Components\BookingForm\BookingForm;
use App\Components\BookingForm\IBookingForm;
use App\Model\BookingModel;
use App\Model\Entity\Booking;
use App\Model\ShopModel;
use App\Model\UserModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\Routers\Route;
use Nette\Application\UI\Form;
use Tracy\Debugger;


class BookingPresenter extends BasePresenter
{
    /**
     * @inject
     * @var EntityManager
     */
    public $entityManager;

    /**
     * @inject
     * @var UserModel
     */
    public $userModel;

    /**
     * @inject
     * @var BookingModel
     */
    public $bookingModel;

    /**
     * @inject
     * @var ShopModel
     */
    public $shopModel;

    /**
     * @var Booking
     */
    private $booking = null;

    public function renderDefault()
    {

    }

    public function actionDefault($shopId = null)
    {
        if($shopId == null)
        {
            $shopId = 1;
        }
        $bookings = $this->bookingModel->getBookingsByShop($shopId)->getResult();
        $shop = $this->shopModel->getShopById($shopId);

        $this->template->bookings = $bookings;
        $this->template->shops = $this->shopModel->getShopsById()->getResult();
        $this->template->thisShop = $shop;
    }

    public function createComponentBookingForm()
    {
        /**
         * @var BookingForm
         */
        $form = $this->context->getByType(IBookingForm::class)->create();
        $form->addSuccessCallback($this->onSucceeded);
        $form->setDefaultEntity($this->booking);

        return $form;
    }

    public function onSucceeded(Form $form, $values)
    {
        $this->flashMessage('Objednávka uložená.');
//        $this->redirect('Homepage:users');
    }

    public function actionDetailBooking($bookingId)
    {
        $this->template->booking = $this->bookingModel->getBookingById($bookingId);
    }

    public function actionEditBooking($bookingId)
    {
        $this->template->booking = $this->booking = $this->bookingModel->getBookingById($bookingId);
    }

    public function actionDeleteBooking($bookingId)
    {
        $this->template->booking = $this->booking = $this->bookingModel->getBookingById($bookingId);
    }

    public function actionBookingsByDate($shopId)
    {
        $shop = $this->shopModel->getShopById($shopId);
        $bookings = $this->bookingModel->getBookingsByShop($shopId)->getResult();
        $book = [];
        foreach($bookings as $booking)
        {
            $bookingsByDate = $this->bookingModel->getBookingByDate($booking->date)->getResult();
            $book[$booking->date->format('Y-m-d')] = $bookingsByDate;
        }
        $reverseBookings = array_reverse($book, true);

        $this->template->bookingsByDate = $reverseBookings;
        $this->template->thisShop = $shop;
        $this->template->shops = $this->shopModel->getShopsById()->getResult();
    }
}