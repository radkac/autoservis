<?php

namespace App\AdminModule\Presenters;

use App\Components\PasswordForm\IPasswordForm;
use App\Model\Entity\User;
use App\Model\UserModel;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;

class UserPresenter extends BasePresenter
{
    /**
     * @inject
     * @var EntityManager
     */
    public $entityManager;

    /**
     * @inject
     * @var UserModel
     */
    public $userModel;

    /**
     * @var User
     */
    private $employee;

    public function startup()
    {
        parent::startup();
        $this->employee = $this->userModel->getUserByUsername($this->getUser()->getIdentity()->username);
    }

    public function actionChangePassword()
    {
        if(!$this->employee)
        {
            $this->flashMessage('Používateľ neexistuje');
            $this->redirect('Homepage:default');
        }

    }

    public function createComponentPasswordForm()
    {
        $form = $this->context->getByType(IPasswordForm::class)->create();
        $form->addSuccessCallback($this->onSucceededPasswordForm);

        $form->setDefaultEntity($this->employee);

        return $form;
    }

    public function onSucceededPasswordForm(Form $form, $values)
    {
        $this->flashMessage('Heslo uložené.');
        $this->redirect('this');
    }
}